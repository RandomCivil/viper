package network

import (
	"io"
	"net"

	"gitlab.com/RandomCivil/common/bytespool"
	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/viper"
)

const largeBufferSize = 32 * 1024

func Pipe(dst io.Writer, src io.Reader, errCh chan error) int64 {
	buf := bytespool.Alloc(int32(largeBufferSize))
	defer bytespool.Free(buf)

	n, err := io.CopyBuffer(dst, src, buf)
	log.DefaultLogger.Debugf("pipe n:%d,err:%v", n, err)
	errCh <- err
	return n
}

func PipeWithNoErrCh(dst io.Writer, src io.Reader) int64 {
	buf := bytespool.Alloc(int32(largeBufferSize))
	defer bytespool.Free(buf)

	n, err := io.CopyBuffer(dst, src, buf)
	log.DefaultLogger.Debugf("handle route pipe n:%d,err:%v", n, err)
	return n
}

func RemoteIP(a viper.Address) string {
	addr := a.RemoteAddr()
	ip, _, _ := net.SplitHostPort(addr.String())
	return ip
}
