package network

import (
	"context"
	"math/rand"
	"net"
	"reflect"
	"sync"
	"testing"
	"time"

	gomock "github.com/golang/mock/gomock"
	"github.com/xtaci/smux"
	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/config"
)

func TestFlexFetch(t *testing.T) {
	ms := &SmuxMockServer{
		StopCh: make(chan struct{}, 1),
		DoneCh: make(chan struct{}, 1),
	}
	go ms.Start()
	<-ms.DoneCh

	smuxConf := &config.SmuxConfig{
		SmuxKeepAliveInterval: 500,
		SmuxKeepAliveTimeout:  2000,
	}

	s := new(Session)
	s.Strategy = "flex"
	s.MaxStreamPerSession = 3
	s.DialFunc = func(ctx context.Context, i int32) *DialTarget {
		dialer := net.Dialer{Timeout: time.Second * 2}
		target, err := dialer.DialContext(ctx, "tcp", ms.ln.Addr().String())
		log.DefaultLogger.Infof("dial target err:%v,i:%d", err, i)
		r := new(DialTarget)
		r.Target = target
		if err != nil {
			r.Done = false
		} else {
			r.Done = true
			if smuxConf == nil {
				r.Conf = &config.SmuxConfig{
					SmuxKeepAliveInterval: 50,
					SmuxKeepAliveTimeout:  1000,
				}
			} else {
				r.Conf = smuxConf
			}
		}
		return r
	}

	tests := []struct {
		input    []int
		expected int
	}{
		{[]int{4, 2, 3, 1, 5}, 2},
		{[]int{1, 4, 3, 5, 2}, 2},
		{[]int{1, 4, 3, 5, 5}, 1},
		{[]int{4, 5, 1, 3, 2}, 1},
		{[]int{4, 5, 5, 5, 5}, 0},
	}
	t.Log(tests)

	mockCtl := gomock.NewController(t)
	for _, tt := range tests {
		s.Ch = make(chan viper.SessionInterface, 5)
		for i := 0; i < 5; i++ {
			mockSession := viper.NewMockSessionInterface(mockCtl)
			mockSession.EXPECT().NumStreams().Return(tt.input[i]).AnyTimes()
			mockSession.EXPECT().SetIndex(int32(i)).AnyTimes()
			mockSession.EXPECT().GetIndex().Return(int32(i)).AnyTimes()
			s.Ch <- mockSession
		}
		r := s.Fetch()
		t.Log(r.NumStreams(), r.GetIndex(), len(s.Ch))
		close(s.Ch)
		if r.NumStreams() != tt.expected {
			t.Fatal("TestFlexFetch error")
		}
	}
	ms.Stop()
}

func TestFlexFetchFail(t *testing.T) {
	s := new(Session)
	s.Strategy = "flex"
	s.MaxStreamPerSession = 3
	s.DialFunc = func(ctx context.Context, i int32) *DialTarget {
		r := new(DialTarget)
		r.Done = false
		return r
	}

	nums := []int{4, 5, 5, 5, 5}
	mockCtl := gomock.NewController(t)
	s.Ch = make(chan viper.SessionInterface, 5)
	for i := 0; i < 5; i++ {
		mockSession := viper.NewMockSessionInterface(mockCtl)
		mockSession.EXPECT().NumStreams().Return(nums[i]).AnyTimes()
		mockSession.EXPECT().SetIndex(int32(i)).AnyTimes()
		mockSession.EXPECT().GetIndex().Return(int32(i)).AnyTimes()
		s.Ch <- mockSession
	}
	r := s.Fetch()
	t.Log(r, len(s.Ch))
	if r != nil {
		t.Fatal("TestFlexFetchFail error")
	}
}

func TestLsfFetch(t *testing.T) {
	s := new(Session)
	s.Strategy = "lsf"
	s.Ch = make(chan viper.SessionInterface, 5)

	mockCtl := gomock.NewController(t)
	tests := []struct {
		input    []int
		expected []int
	}{
		{[]int{4, 2, 3, 1, 5}, []int{1, 2, 3, 4, 5}},
		{[]int{4, 1, 3, 1, 5}, []int{1, 1, 3, 4, 5}},
		{[]int{4, 1, 2, 1, 5}, []int{1, 1, 2, 4, 5}},
	}
	t.Log(tests)
	for _, tt := range tests {
		for i := 0; i < 5; i++ {
			mockSession := viper.NewMockSessionInterface(mockCtl)
			mockSession.EXPECT().NumStreams().Return(tt.input[i]).AnyTimes()
			mockSession.EXPECT().SetIndex(int32(i)).AnyTimes()
			mockSession.EXPECT().GetIndex().Return(int32(i)).AnyTimes()
			s.Ch <- mockSession
		}
		result := make([]int, 5)
		for i := 0; i < 5; i++ {
			r := s.Fetch()
			result[i] = int(r.NumStreams())
		}
		t.Log(result)
		if !reflect.DeepEqual(result, tt.expected) {
			t.Fatal("TestLsfFetch error")
		}
	}
}

func TestMakeStream(t *testing.T) {
	ms := &SmuxMockServer{
		StopCh: make(chan struct{}, 1),
		DoneCh: make(chan struct{}, 1),
	}
	go ms.Start()
	<-ms.DoneCh

	smuxConf := &config.SmuxConfig{
		SmuxKeepAliveInterval: 500,
		SmuxKeepAliveTimeout:  2000,
	}

	dialFunc := func(ctx context.Context, i int32) *DialTarget {
		dialer := net.Dialer{Timeout: time.Second * 2}
		target, err := dialer.DialContext(ctx, "tcp", ms.ln.Addr().String())
		log.DefaultLogger.Infof("dial target err:%v,i:%d", err, i)
		r := new(DialTarget)
		r.Target = target
		if err != nil {
			r.Done = false
		} else {
			r.Done = true
			if smuxConf == nil {
				r.Conf = &config.SmuxConfig{
					SmuxKeepAliveInterval: 50,
					SmuxKeepAliveTimeout:  1000,
				}
			} else {
				r.Conf = smuxConf
			}
		}
		return r
	}

	mockCtl := gomock.NewController(t)
	mockSession := viper.NewMockSessionInterface(mockCtl)
	mockSession.EXPECT().NumStreams().Return(7).AnyTimes()
	mockSession.EXPECT().SetIndex(gomock.Any())
	mockSession.EXPECT().GetIndex().Return(int32(1)).AnyTimes()
	mockSession.EXPECT().OpenStream().Return(nil, nil).AnyTimes()
	mockSession.EXPECT().SetConcreteSession(gomock.Any()).Do(
		func(arg viper.ConcreteSessionInterface) {
			log.DefaultLogger.Infof("concreate session,type:%v,local addr:%s,remote addr:%s", reflect.TypeOf(arg), arg.LocalAddr().String(), arg.RemoteAddr().String())
		},
	).AnyTimes()

	tests := []struct {
		strategy string
		mockNum  int
	}{
		{"flex", 0},
		{"lsf", 0},
		{"flex", 5},
	}

	sess := new(Session)
	sess.Ch = make(chan viper.SessionInterface, 5)
	sess.DialFunc = dialFunc
	sess.MaxStreamPerSession = 3

	for _, tt := range tests {
		t.Log(tt)
		sess.Strategy = tt.strategy
		if tt.mockNum > 0 {
			wg := sync.WaitGroup{}
			wg.Add(tt.mockNum)
			for i := 0; i < tt.mockNum; i++ {
				newIndex := sid.index()
				mockSession1 := viper.NewMockSessionInterface(mockCtl)
				mockSession1.EXPECT().NumStreams().Return(i).AnyTimes()
				mockSession1.EXPECT().SetIndex(newIndex).AnyTimes()
				mockSession1.EXPECT().GetIndex().Return(newIndex).AnyTimes()
				mockSession1.EXPECT().OpenStream().Return(nil, nil).AnyTimes()
				mockSession1.EXPECT().SetConcreteSession(gomock.Any()).Do(
					func(arg viper.ConcreteSessionInterface) {
						log.DefaultLogger.Infof("concreate session,type:%v,local addr:%s,remote addr:%s", reflect.TypeOf(arg), arg.LocalAddr().String(), arg.RemoteAddr().String())
					},
				).AnyTimes()
				go func() {
					r := rand.Intn(50)
					time.Sleep(time.Duration(r) * time.Millisecond)
					sess.Ch <- mockSession1
					wg.Done()
				}()
			}
			wg.Wait()
		}
		sess.MakeStream(mockSession, tt.strategy)
	}
	ms.Stop()
}

func TestSessCleanTimer(t *testing.T) {
	mockCtl := gomock.NewController(t)
	tests := []struct {
		nums     []int
		expected []int
	}{
		{[]int{5, 0, 0}, []int{3, 3, 1, 1}},
		{[]int{0, 0, 0}, []int{3, 3, 0, 0}},
		{[]int{}, []int{-1, -1, -1, -1}},
	}
	for _, tt := range tests {
		sm := NewSessMap()
		for _, n := range tt.nums {
			mockSession := viper.NewMockSessionInterface(mockCtl)
			mockSession.EXPECT().NumStreams().Return(n).AnyTimes()
			mockSession.EXPECT().Close().Return(nil).AnyTimes()
			sm.Set("127.0.0.1", mockSession, 100)
		}

		wait := make(chan struct{})
		i := 0
		r := make([]int, 0)
		tick := time.NewTicker(time.Millisecond * time.Duration(40))
		defer tick.Stop()

		go func(t *testing.T) {
			for {
				select {
				case <-tick.C:
					sm.mu.RLock()
					set, ok := sm.get("127.0.0.1")
					var num int
					if !ok {
						num = -1
					} else {
						num = len(set)
					}
					sm.mu.RUnlock()

					t.Logf("set len %d index %d", num, i)
					r = append(r, num)
					i++
					if i == 4 {
						tick.Stop()
						t.Log(r, tt.expected)
						if !reflect.DeepEqual(r, tt.expected) {
							t.Fatal("TestSessCleanTimer error")
						}
						wait <- struct{}{}
						return
					}
				}
			}
		}(t)
		<-wait
	}
}

type SmuxMockServer struct {
	StopCh, DoneCh chan struct{}
	ln             net.Listener
}

func (m *SmuxMockServer) Stop() {
	m.StopCh <- struct{}{}
}

func (m *SmuxMockServer) handleConn(conn net.Conn) {
	session, err := smux.Server(conn, nil)
	if err != nil {
		log.DefaultLogger.Errorf("smux server err:%v", err.Error())
		return
	}

	for {
		if stream, err := session.AcceptStream(); err == nil {
			go func() {
				b := make([]byte, 2)
				n, err := stream.Read(b)
				log.DefaultLogger.Infof("accept stream read:%v,err:%v", n, err)
				if err != nil {
					return
				}
				stream.Write(b[:n])
			}()
		} else {
			session.Close()
			break
		}
	}
}

func (m *SmuxMockServer) listenLoop() {
	for {
		conn, err := m.ln.Accept()
		if err != nil {
			log.DefaultLogger.Errorf("mock server accept err:%v", err)
			return
		}
		go m.handleConn(conn)
	}
}

func (m *SmuxMockServer) Start() {
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		log.DefaultLogger.Errorf("mock server listen err:%v", err)
		return
	}
	m.ln = l
	m.DoneCh <- struct{}{}
	log.DefaultLogger.Infof("server start at %s", l.Addr().String())
	go m.listenLoop()
	select {
	case <-m.StopCh:
		return
	default:
	}
}
