package network

import (
	"net"
	"testing"
	"time"
)

var host = "www.baidu.com"

func TestDnsCache(t *testing.T) {
	dnsCache := NewDnsCache(2, 1)
	dnsCache.SetDnsPriority(true)
	go dnsCache.CleanTimer(2)

	var (
		ip  net.IP
		err error
	)
	for i := 0; i < 3; i++ {
		ip, _, err = dnsCache.QueryDns(host)
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("domain:%s,ip:%s", host, ip)
	}

	time.Sleep(5 * time.Second)
	dnsCache.SetDnsPriority(false)
	ip, _, err = dnsCache.QueryDns("www.baidu.com")
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("domain:%s,ip:%s", host, ip)
}
