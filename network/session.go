package network

//go:generate go run github.com/golang/mock/mockgen -destination ../mock_session.go -package viper gitlab.com/RandomCivil/viper SessionInterface

import (
	"context"
	"errors"
	"io"
	"net"
	"sync"
	"sync/atomic"
	"time"

	quicGo "github.com/quic-go/quic-go"
	"github.com/xtaci/smux"
	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/config"
)

const (
	MAX_SESSION_NUM = 20
)

var sid = new(sessionId)

type sessionId struct {
	i int32
}

func (si *sessionId) index() int32 {
	return atomic.AddInt32(&si.i, 1)
}

type QuicConcreteSession struct {
	quicGo.Connection
}

func (s QuicConcreteSession) Close() error {
	return s.CloseWithError(1, "quic go session closed")
}

type QuicSession struct {
	Se         QuicConcreteSession
	Index      int32
	numStreams int32
}

func (s *QuicSession) AcceptStream() (io.ReadWriteCloser, error) {
	return s.Se.AcceptStream(context.Background())
}

func (s *QuicSession) RemoteAddr() net.Addr {
	return s.Se.RemoteAddr()
}

func (s *QuicSession) LocalAddr() net.Addr {
	return s.Se.LocalAddr()
}

func (s *QuicSession) Close() error {
	return s.Se.Close()
}

func (s *QuicSession) NumStreams() int {
	return int(atomic.AddInt32(&s.numStreams, 0))
}

func (s *QuicSession) GetIndex() int32 {
	return atomic.AddInt32(&s.Index, 0)
}

func (s *QuicSession) SetIndex(index int32) {
	s.Index = index
}

func (s *QuicSession) GetSession() viper.ConcreteSessionInterface {
	return s.Se
}

func (s *QuicSession) SetConcreteSession(cs viper.ConcreteSessionInterface) {
	s.Se = cs.(QuicConcreteSession)
}

func (s *QuicSession) OpenStream() (io.ReadWriteCloser, error) {
	log.DefaultLogger.Infof("quic session %d has stream num %d", s.Index, s.numStreams)
	atomic.AddInt32(&s.numStreams, 1)
	return s.Se.OpenStreamSync(context.Background())
}

func (s *QuicSession) CloseStream(stream io.Closer) error {
	//log.DefaultLogger.Infof("quic session %d has stream num %d is about close", s.Index, s.numStreams)
	atomic.AddInt32(&s.numStreams, -1)
	return stream.Close()
}

type SmuxSession struct {
	Se    *smux.Session
	Index int32
}

func (s *SmuxSession) AcceptStream() (io.ReadWriteCloser, error) {
	return s.Se.AcceptStream()
}

func (s *SmuxSession) RemoteAddr() net.Addr {
	return s.Se.RemoteAddr()
}

func (s *SmuxSession) LocalAddr() net.Addr {
	return s.Se.LocalAddr()
}

func (s *SmuxSession) Close() error {
	return s.Se.Close()
}

func (s *SmuxSession) NumStreams() int {
	return s.Se.NumStreams()
}

func (s *SmuxSession) GetIndex() int32 {
	return atomic.AddInt32(&s.Index, 0)
}

func (s *SmuxSession) SetIndex(index int32) {
	s.Index = index
}

func (s *SmuxSession) GetSession() viper.ConcreteSessionInterface {
	return s.Se
}
func (s *SmuxSession) SetConcreteSession(cs viper.ConcreteSessionInterface) {
	s.Se = cs.(*smux.Session)
}

func (s *SmuxSession) OpenStream() (io.ReadWriteCloser, error) {
	return s.Se.OpenStream()
}

func (s *SmuxSession) CloseStream(stream io.Closer) error {
	return stream.Close()
}

type SessionConfig struct {
	Strategy            string
	Num                 int
	MaxStreamPerSession int
}

type ServerAddr struct {
	Addr, IpType string
}

type Session struct {
	Strategy            string
	Ch                  chan viper.SessionInterface
	DialFunc            func(ctx context.Context, i int32) *DialTarget
	MaxStreamPerSession int
}

func NewSession(config *SessionConfig, dialFunc func(ctx context.Context, i int32) *DialTarget) (*Session, error) {
	var err error
	var sessionCh chan viper.SessionInterface
	s := &Session{
		Strategy:            config.Strategy,
		DialFunc:            dialFunc,
		MaxStreamPerSession: config.MaxStreamPerSession,
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	switch config.Strategy {
	case "fixed", "lsf":
		var i int32
		var s viper.ConcreteSessionInterface
		var once sync.Once
		var sessionWg = sync.WaitGroup{}
		var sessLock sync.Mutex
		sessionCh = make(chan viper.SessionInterface, config.Num)

		sessionWg.Add(config.Num)
		for i = 0; i < int32(config.Num); i++ {
			sessLock.Lock()
			go func(ctx context.Context, i int32) {
				errCh := make(chan error)
				done := make(chan struct{})
				go func(ctx context.Context) {
					s = MakeSession(ctx, i, dialFunc)
					if s == nil {
						errCh <- errors.New("connect server fail")
						onceBody := func() {
							log.DefaultLogger.Errorf("start cancel coroutine %d", i)
							cancel()
						}
						once.Do(onceBody)
					} else {
						if ss, ok := s.(*smux.Session); ok {
							sessionCh <- &SmuxSession{Se: ss, Index: i}
						}
						if qs, ok1 := s.(QuicConcreteSession); ok1 {
							sessionCh <- &QuicSession{Se: qs, Index: i}
						}
						done <- struct{}{}
					}
				}(ctx)
				select {
				case e := <-errCh:
					err = e
				case <-done:
				}
				sessLock.Unlock()
				sessionWg.Done()
			}(ctx, i)
		}
		sessionWg.Wait()
		log.DefaultLogger.Infof("all sessions done")
	case "flex":
		s := MakeSession(ctx, 1, dialFunc)
		if s == nil {
			return nil, errors.New("connect server fail")
		}
		sessionCh = make(chan viper.SessionInterface, MAX_SESSION_NUM)
		index := sid.index()
		if ss, ok := s.(*smux.Session); ok {
			sessionCh <- &SmuxSession{Se: ss, Index: index}
		}
		if qs, ok1 := s.(QuicConcreteSession); ok1 {
			sessionCh <- &QuicSession{Se: qs, Index: index}
		}
	}
	s.Ch = sessionCh
	return s, err
}

type DialTarget struct {
	Target viper.ConcreteSessionInterface
	Done   bool
	Conf   *config.SmuxConfig
}

func MakeSession(ctx context.Context, i int32, dialFunc func(ctx context.Context, i int32) *DialTarget) (sess viper.ConcreteSessionInterface) {
	ch := make(chan *DialTarget, 1)
	go func() {
		ch <- dialFunc(ctx, i)
	}()
	select {
	case <-ctx.Done():
		log.DefaultLogger.Infof("context cancel session %d", i)
		return nil
	case dt := <-ch:
		if dt.Done {
			switch t := dt.Target.(type) {
			case net.Conn:
				conf := smux.DefaultConfig()
				conf.KeepAliveInterval = time.Duration(dt.Conf.SmuxKeepAliveInterval) * time.Millisecond
				conf.KeepAliveTimeout = time.Duration(dt.Conf.SmuxKeepAliveTimeout) * time.Millisecond
				sess, _ = smux.Client(t, conf)
			case viper.ConcreteSessionInterface:
				sess = t
			}
			return
		} else {
			sess = nil
			return
		}
	}
}

func (se *Session) Fetch() viper.SessionInterface {
	var (
		s       viper.SessionInterface
		i       int32
		isFirst bool = true
	)

	//round robin
	if se.Strategy == "fixed" {
		return <-se.Ch
	}

	mu := sync.Mutex{}
	mu.Lock()
	defer mu.Unlock()

	switch se.Strategy {
	//least stream first
	case "lsf":
		var min int
		var r viper.SessionInterface
		for {
			s = <-se.Ch
			if isFirst {
				i = s.GetIndex()
				r = s
				min = s.NumStreams()
				isFirst = false
				se.Ch <- s
				continue
			}
			log.DefaultLogger.Infof("cur:%d min:%d cur_i:%d i:%d len:%d", s.NumStreams(), min, s.GetIndex(), i, len(se.Ch))
			if !isFirst && s.GetIndex() == i {
				break
			}
			if s.NumStreams() < min {
				se.Ch <- r
				min = s.NumStreams()
				r = s
			} else {
				se.Ch <- s
			}
		}
		log.DefaultLogger.Infof("choose session %d,num:%d", r.GetIndex(), r.NumStreams())
		return r
	case "flex":
		for {
			s = <-se.Ch
			log.DefaultLogger.Infof("iter session %d stream num %d,isFirst:%t,start:%d", s.GetIndex(), s.NumStreams(), isFirst, i)
			if isFirst {
				i = s.GetIndex()
				isFirst = false
				se.Ch <- s
				continue
			}
			if s.NumStreams() < se.MaxStreamPerSession {
				log.DefaultLogger.Infof("session %d stream num %d < %d", s.GetIndex(), s.NumStreams(), se.MaxStreamPerSession)
				break
			}
			if !isFirst && s.GetIndex() == i {
				log.DefaultLogger.Debugf("before create session,start:%d", i)
				newIndex := sid.index()
				newSess := MakeSession(context.Background(), newIndex, se.DialFunc)
				if newSess == nil {
					log.DefaultLogger.Infof("create session %d fail", newIndex)
					return nil
				}
				var newSession viper.SessionInterface
				if ss, ok := newSess.(*smux.Session); ok {
					newSession = &SmuxSession{Se: ss, Index: newIndex}
				}
				if qs, ok1 := newSess.(QuicConcreteSession); ok1 {
					newSession = &QuicSession{Se: qs, Index: newIndex}
				}
				log.DefaultLogger.Infof("fetch create session %d", newIndex)
				se.Ch <- s
				s = newSession
				break
			}
			se.Ch <- s
		}
		return s
	default:
		return nil
	}
}

func (s *Session) MakeStream(session viper.SessionInterface, strategy string) (viper.SessionInterface, io.ReadWriteCloser) {
	for {
		var newSess viper.ConcreteSessionInterface
		if strategy != "flex" {
			newSess = MakeSession(context.Background(), session.GetIndex(), s.DialFunc)
			if newSess == nil {
				log.DefaultLogger.Infof("create %s session %d fail", strategy, session.GetIndex())
				continue
			}
			session.SetConcreteSession(newSess)
		} else {
			//no one left,make session at current session index
			if len(s.Ch) == 0 {
				i := sid.index()
				newSess = MakeSession(context.Background(), i, s.DialFunc)
				if newSess == nil {
					log.DefaultLogger.Infof("create %s session %d fail", strategy, i)
					continue
				}
				session.SetIndex(i)
				log.DefaultLogger.Infof("make stream create session %d,%+v", i, newSess)
				session.SetConcreteSession(newSess)
			} else { //at least one remain,fetch one
				session = s.Fetch()
				if session == nil {
					continue
				}
			}
		}
		stream, err := session.OpenStream()
		log.DefaultLogger.Infof("make stream err:%v", err)
		if err == nil {
			return session, stream
		}
	}
}

func (se *Session) Close() {
	for {
		select {
		case session := <-se.Ch:
			session.GetSession().Close()
		default:
			return
		}
	}
}
