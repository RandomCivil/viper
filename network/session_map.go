package network

import (
	"sync"
	"time"

	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/viper"
)

type SessMap struct {
	mu sync.RWMutex
	m  map[string]map[viper.SessionInterface]struct{}
}

func NewSessMap() *SessMap {
	return &SessMap{
		mu: sync.RWMutex{},
		m:  make(map[string]map[viper.SessionInterface]struct{}),
	}
}

func (s *SessMap) get(ip string) (map[viper.SessionInterface]struct{}, bool) {
	mm, ok := s.m[ip]
	return mm, ok
}

func (s *SessMap) Set(ip string, sess viper.SessionInterface, interval int) {
	s.mu.Lock()
	if m, ok := s.m[ip]; !ok {
		mm := make(map[viper.SessionInterface]struct{})
		mm[sess] = struct{}{}
		s.m[ip] = mm
		go s.cleanTicker(ip, interval)
	} else {
		m[sess] = struct{}{}
	}
	s.mu.Unlock()
}

func (s *SessMap) Del(ip string, sess viper.SessionInterface) {
	s.mu.Lock()
	if m, ok := s.m[ip]; ok {
		delete(m, sess)
	}
	s.mu.Unlock()
}

func (s *SessMap) delAll(ip string) {
	s.m[ip] = nil
	delete(s.m, ip)
}

func (s *SessMap) cleanTicker(ip string, interval int) {
	tick := time.NewTicker(time.Millisecond * time.Duration(interval))
	defer tick.Stop()

	for {
		select {
		case <-tick.C:
			s.mu.Lock()
			sessSet, _ := s.get(ip)
			if len(sessSet) == 0 {
				s.delAll(ip)
				s.mu.Unlock()
				return
			}

			var delSess []viper.SessionInterface
			for sess, _ := range sessSet {
				n := sess.NumStreams()
				log.DefaultLogger.Infof("session num:%d", n)
				if n == 0 {
					delSess = append(delSess, sess)
					sess.Close()
				}
			}
			log.DefaultLogger.Infof("del session %d", len(delSess))

			for _, s := range delSess {
				delete(sessSet, s)
			}
			s.mu.Unlock()
		}
	}
}
