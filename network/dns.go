package network

import (
	"errors"
	"net"
	"sync"
	"time"

	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/viper/internal"
)

var (
	ipTypeMap = map[string]uint8{
		"ip6": internal.IPV6_TYPE,
		"ip":  internal.IPV4_TYPE,
	}
	ErrDomainNotParsed = errors.New("domain not parsed")
)

type address struct {
	addr *net.IPAddr
}

func (a *address) toMap(ipType string) (net.IP, uint8) {
	var ip net.IP
	switch ipType {
	case "ip6":
		ip = a.addr.IP.To16()
	case "ip":
		ip = a.addr.IP.To4()
	}
	return ip, ipTypeMap[ipType]
}

type DnsCache struct {
	m         sync.Map
	Threshold int64
	priority  []string
}

type IpCache struct {
	Ip         net.IP
	IpType     uint8
	UpdateTime time.Time
}

func NewDnsCache(interval, threshold int) *DnsCache {
	log.DefaultLogger.Infof("interval:%v,threshold:%v", interval, threshold)
	c := &DnsCache{
		m:         sync.Map{},
		priority:  []string{"ip", "ip6"},
		Threshold: int64(threshold),
	}
	go c.CleanTimer(interval)
	return c
}

func (c *DnsCache) SetDnsPriority(ipv6First bool) {
	switch ipv6First {
	case false:
		c.priority = []string{"ip", "ip6"}
	case true:
		c.priority = []string{"ip6", "ip"}
	}
}

type dnsAddr struct {
	err  error
	addr *net.IPAddr
}

func (c *DnsCache) QueryDns(domain string) (net.IP, uint8, error) {
	var addr *net.IPAddr
	var err error

	dnsAddrCh := make(chan *dnsAddr)
	for _, ipType := range c.priority {
		go func() {
			addr, err = net.ResolveIPAddr(ipType, string(domain))
			dnsAddrCh <- &dnsAddr{addr: addr, err: err}
		}()
		select {
		case dc := <-dnsAddrCh:
			if dc.err != nil {
				log.DefaultLogger.Infof("query dns ipType:%s,err:%v", ipType, dc.err)
			} else {
				a := address{addr: addr}
				ip, t := a.toMap(ipType)
				return ip, t, nil
			}
		}
	}

	return nil, 0, ErrDomainNotParsed
}

func (c *DnsCache) Set(domain string, cache *IpCache) {
	c.m.Store(domain, cache)
}

func (c *DnsCache) Get(domain string) *IpCache {
	cache, find := c.m.Load(domain)
	if !find {
		return nil
	}
	return cache.(*IpCache)
}

func (c *DnsCache) CleanTimer(interval int) {
	tick := time.NewTicker(time.Second * time.Duration(interval))
	for {
		select {
		case <-tick.C:
			c.m.Range(c.rangeFunc)
		}
	}
}

func (c *DnsCache) rangeFunc(domain, cache interface{}) bool {
	_cache := cache.(*IpCache)
	if time.Now().Unix()-_cache.UpdateTime.Unix() > c.Threshold {
		c.m.Delete(domain)
		log.DefaultLogger.Infof("%s cache expire", domain)
	}
	return true
}
