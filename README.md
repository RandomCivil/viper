# viper

proxy via SOCKS5

[![pipeline status](https://gitlab.com/RandomCivil/viper/badges/master/pipeline.svg)](https://gitlab.com/RandomCivil/viper/commits/master) [![codecov](https://codecov.io/gl/RandomCivil/viper/branch/master/graph/badge.svg?token=b4rN3WgQTs)](https://codecov.io/gl/RandomCivil/viper)

[DOC](https://randomcivil.gitlab.io/doc/viper/)
