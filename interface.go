package viper

import (
	"io"
	"net"

	"gitlab.com/RandomCivil/common/buf"
)

type SessionInterface interface {
	AcceptStream() (io.ReadWriteCloser, error)
	OpenStream() (io.ReadWriteCloser, error)
	CloseStream(stream io.Closer) error
	NumStreams() int
	io.Closer
	Address
	GetIndex() int32
	SetIndex(i int32)
	GetSession() ConcreteSessionInterface
	SetConcreteSession(cs ConcreteSessionInterface)
}

//smux.Session,quicGo.Session
type ConcreteSessionInterface interface {
	io.Closer
	Address
}

type Address interface {
	LocalAddr() net.Addr
	RemoteAddr() net.Addr
}

type Writer interface {
	io.Writer
}

type Reader interface {
	io.Reader
}

type Cipher interface {
	InitEncrypt() (iv []byte, err error)
	InitDecrypt(iv []byte) (err error)
	Encrypt(dst, src []byte) int
	Decrypt(dst, src []byte) (int, error)
	IVLen() int
	IV() []byte
	Overhead() int
	Buf() *buf.Buffer
	Release()
}
