package encrypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"io"
	"sync"

	"gitlab.com/RandomCivil/common/buf"
	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/viper"
	"golang.org/x/crypto/chacha20poly1305"
)

type aeadCipherInfo struct {
	keyLen    int
	ivLen     int
	nonceSize int
	newStream func(key []byte) (cipher.AEAD, error)
}

type AeadCipher struct {
	enc   cipher.AEAD
	dec   cipher.AEAD
	key   []byte
	Info  *aeadCipherInfo
	iv    []byte
	nonce []byte
	pwd   string
	buf   *buf.Buffer
}

var (
	AeadCipherMethod = map[string]*aeadCipherInfo{
		"aes-gcm":           {16, 16, 12, newAesGcmStream},
		"chacha20-poly1305": {chacha20poly1305.KeySize, 32, chacha20poly1305.NonceSize, chacha20poly1305.New},
	}
	ErrAeadDec = errors.New("aead cipher decrypt fail")
	noncePool  = sync.Pool{
		New: func() interface{} {
			return make([]byte, 12)
		},
	}
)

func increment(b []byte) {
	for i := range b {
		b[i]++
		if b[i] != 0 {
			return
		}
	}
}

func NewAeadCipher(method, pwd string) viper.Cipher {
	mi := AeadCipherMethod[method]
	buf := buf.New()
	//nonce := make([]byte, mi.nonceSize)
	nonce := noncePool.Get().([]byte)
	return &AeadCipher{Info: mi, pwd: pwd, nonce: nonce, buf: buf}
}

func newAesGcmStream(key []byte) (cipher.AEAD, error) {
	blk, _ := aes.NewCipher(key)
	return cipher.NewGCM(blk)
}

func (c *AeadCipher) InitEncrypt() (iv []byte, err error) {
	iv = c.buf.BytesTo(int32(c.Info.ivLen))
	c.buf.Advance(int32(c.Info.ivLen))
	io.ReadFull(rand.Reader, iv)
	c.iv = iv

	key := c.buf.BytesTo(int32(c.Info.keyLen))
	hkdfSHA1([]byte(c.pwd), iv, []byte(subkey), key)
	c.enc, err = c.Info.newStream(key)
	return iv, err
}

func (c *AeadCipher) InitDecrypt(iv []byte) error {
	key := c.buf.BytesTo(int32(c.Info.keyLen))
	//c.buf.Advance(int32(c.Info.keyLen))

	hkdfSHA1([]byte(c.pwd), iv, []byte(subkey), key)
	var err error
	c.iv = iv
	c.dec, err = c.Info.newStream(key)
	return err
}

func (c *AeadCipher) Encrypt(dst, src []byte) int {
	increment(c.nonce)
	log.DefaultLogger.Debugf("enc iv:%v,nonce:%v", c.iv, c.nonce)
	b := c.enc.Seal(dst[:0], c.nonce, src, nil)
	return len(b)
}

func (c *AeadCipher) Decrypt(dst, src []byte) (int, error) {
	increment(c.nonce)
	log.DefaultLogger.Debugf("dec iv:%v,nonce:%v", c.iv, c.nonce)
	b, err := c.dec.Open(dst[:0], c.nonce, src, nil)
	if err != nil {
		return 0, ErrAeadDec
	}
	return len(b), nil
}

func (c *AeadCipher) IVLen() int {
	return c.Info.ivLen
}

func (c *AeadCipher) Overhead() int {
	if c.dec != nil {
		return c.dec.Overhead()
	} else {
		return c.enc.Overhead()
	}
}

func (c *AeadCipher) IV() []byte {
	return c.iv
}

func (c *AeadCipher) Buf() *buf.Buffer {
	return c.buf
}

func (c *AeadCipher) Release() {
	for i := range c.nonce {
		if c.nonce[i] == 0 {
			break
		}
		c.nonce[i] = 0
	}
	noncePool.Put(c.nonce)
	c.buf.Release()
}
