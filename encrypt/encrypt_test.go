package encrypt_test

import (
	"bytes"
	"testing"

	"gitlab.com/RandomCivil/viper/encrypt"
	"gitlab.com/RandomCivil/viper/internal"
	"gitlab.com/RandomCivil/viper/socks5"
)

const text = "lovelittlefatcat"

func TestAeadCipher(t *testing.T) {
	for k, _ := range encrypt.AeadCipherMethod {
		encCipher := socks5.NewEncCipher(k, "123", "aead")
		iv := encCipher.IV()
		encBytes := make([]byte, len(text)+encCipher.Overhead()+internal.TS_LEN)
		nr := encCipher.Encrypt(encBytes, []byte(text))

		stream := bytes.NewBuffer([]byte(iv))
		stream.Write(encBytes[:nr])
		decCipher := socks5.NewDecCipher(stream, k, "123", "aead")
		decBytes := make([]byte, len(text)+decCipher.Overhead()+internal.TS_LEN)
		nr, err := decCipher.Decrypt(decBytes, stream.Bytes())
		if err != nil {
			t.Fatalf("%s cipher decrypt fail", k)
		}
		if string(decBytes[:nr]) != text {
			t.Fatalf("test %s cipher fail", k)
		}
	}
}

func TestStreamCipher(t *testing.T) {
	for k, _ := range encrypt.StreamCipherMethod {
		encCipher := socks5.NewEncCipher(k, "123", "stream")
		iv := encCipher.IV()

		n := len(text)
		cipherBuf := make([]byte, n)
		originBuf := make([]byte, n)

		encCipher.Encrypt(cipherBuf, []byte(text))

		stream := bytes.NewBuffer([]byte(iv))
		stream.Write(cipherBuf)

		decCipher := socks5.NewDecCipher(stream, k, "123", "stream")
		decCipher.Decrypt(originBuf, stream.Bytes())

		if string(originBuf) != text {
			t.Fatalf("test %s cipher fail", k)
		}
	}
}

func TestCheckParam(t *testing.T) {
	tests := []struct {
		name, pwd  string
		cipherType string
		hasError   bool
	}{
		{"aes-128-cfb", "", "", true},
		{"aes-128-cfb", "1", "stream", false},
		{"aes-gcm", "1", "aead", false},
		{"cfb-128", "1", "", true},
	}
	for _, tt := range tests {
		typo, err := encrypt.CheckParam(tt.name, tt.pwd)
		if !tt.hasError {
			if err != nil {
				t.Fatalf("TestCheckParam fail,tt:%v,typo:%s,err:%v", tt, typo, err)
			}
			if typo != tt.cipherType {
				t.Fatalf("TestCheckParam fail,tt:%v,typo:%s,err:%v", tt, typo, err)
			}
		} else {
			if err == nil {
				t.Fatalf("TestCheckParam fail,tt:%v,typo:%s,err:%v", tt, typo, err)
			}
			if typo != "" {
				t.Fatalf("TestCheckParam fail,tt:%v,typo:%s,err:%v", tt, typo, err)
			}
		}
	}
}
