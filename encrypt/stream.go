package encrypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"

	"github.com/aead/chacha20"
	"gitlab.com/RandomCivil/common/buf"
	"gitlab.com/RandomCivil/viper"
)

type DecOrEnc int

const (
	Decrypt DecOrEnc = iota
	Encrypt
)

func newStream(block cipher.Block, key, iv []byte, doe DecOrEnc) (cipher.Stream, error) {
	if doe == Encrypt {
		return cipher.NewCFBEncrypter(block, iv), nil
	} else {
		return cipher.NewCFBDecrypter(block, iv), nil
	}
}

func newAESCFBStream(key, iv []byte, doe DecOrEnc) (cipher.Stream, error) {
	block, _ := aes.NewCipher(key)
	return newStream(block, key, iv, doe)
}

func newChaCha20Stream(key, iv []byte, _ DecOrEnc) (cipher.Stream, error) {
	return chacha20.NewCipher(iv, key)
}

func newChaCha20IETFStream(key, iv []byte, _ DecOrEnc) (cipher.Stream, error) {
	return chacha20.NewCipher(iv, key)
}

type streamCipherInfo struct {
	keyLen    int
	ivLen     int
	newStream func(key, iv []byte, doe DecOrEnc) (cipher.Stream, error)
}

var StreamCipherMethod = map[string]*streamCipherInfo{
	"aes-128-cfb":   {16, 16, newAESCFBStream},
	"aes-192-cfb":   {24, 16, newAESCFBStream},
	"chacha20":      {32, 8, newChaCha20Stream},
	"chacha20-ietf": {32, 12, newChaCha20IETFStream},
}

type StreamCipher struct {
	enc  cipher.Stream
	dec  cipher.Stream
	Info *streamCipherInfo
	iv   []byte
	pwd  string
	buf  *buf.Buffer
}

func NewStreamCipher(method, pwd string) viper.Cipher {
	mi := StreamCipherMethod[method]
	return &StreamCipher{Info: mi, pwd: pwd, buf: buf.New()}
}

func (c *StreamCipher) InitEncrypt() (iv []byte, err error) {
	iv = c.buf.BytesTo(int32(c.Info.ivLen))
	c.buf.Advance(int32(c.Info.ivLen))
	io.ReadFull(rand.Reader, iv)

	key := c.buf.BytesTo(int32(c.Info.keyLen))
	hkdfSHA1([]byte(c.pwd), iv, []byte(subkey), key)
	c.iv = iv
	c.enc, err = c.Info.newStream(key, iv, Encrypt)
	return
}

func (c *StreamCipher) InitDecrypt(iv []byte) (err error) {
	key := c.buf.BytesTo(int32(c.Info.keyLen))
	hkdfSHA1([]byte(c.pwd), iv, []byte(subkey), key)
	c.iv = iv
	c.dec, err = c.Info.newStream(key, iv, Decrypt)
	return
}

func (c *StreamCipher) Encrypt(dst, src []byte) int {
	c.enc.XORKeyStream(dst, src)
	return len(src)
}

func (c *StreamCipher) Decrypt(dst, src []byte) (int, error) {
	c.dec.XORKeyStream(dst, src)
	return len(src), nil
}

func (c *StreamCipher) IVLen() int {
	return c.Info.ivLen
}

func (c *StreamCipher) Overhead() int {
	return 0
}

func (c *StreamCipher) IV() []byte {
	return c.iv
}

func (c *StreamCipher) Buf() *buf.Buffer {
	return c.buf
}

func (c *StreamCipher) Release() {
	c.buf.Release()
}
