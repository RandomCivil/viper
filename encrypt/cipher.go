package encrypt

import (
	"crypto/sha1"
	"errors"
	"io"

	"gitlab.com/RandomCivil/viper"
	"golang.org/x/crypto/hkdf"
)

var subkey = "viper-sk"

func NewCipher(name, pwd, cipherType string) (c viper.Cipher) {
	switch cipherType {
	case "stream":
		c = NewStreamCipher(name, pwd)
	case "aead":
		c = NewAeadCipher(name, pwd)
	}
	return
}

func CheckParam(name, pwd string) (string, error) {
	if pwd == "" {
		return "", errors.New("empty password")
	}
	for k, _ := range AeadCipherMethod {
		if k == name {
			return "aead", nil
		}
	}
	for k, _ := range StreamCipherMethod {
		if k == name {
			return "stream", nil
		}
	}
	return "", errors.New("no such cipher method")
}

func hkdfSHA1(secret, salt, info, outkey []byte) {
	r := hkdf.New(sha1.New, secret, salt, info)
	io.ReadFull(r, outkey)
}
