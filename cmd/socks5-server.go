package main

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/internal"
	"gitlab.com/RandomCivil/viper/socks5"
	"gopkg.in/yaml.v2"
)

var socks5ServerConfig = &config.Socks5ServerConfig{}

func init() {
	socks5ServerCmd := &cobra.Command{Use: "server"}
	socks5ServerCmd.Flags().IntVarP(&socks5ServerConfig.Listen, "listen", "l", 3000, "client listening port")
	socks5ServerCmd.Flags().IntVarP(&socks5ServerConfig.DnsCleanInterval, "dns-clean-interval", "", internal.DNS_CACHE_EXPIRE, "dns clean interval")
	socks5ServerCmd.Flags().StringVarP(&socks5ServerConfig.Pwd, "pwd", "p", "foobar", "password")
	socks5ServerCmd.Flags().StringVarP(&socks5ServerConfig.Cipher, "cipher", "c", "aes-gcm", "cipher method,stream cipher:aes-128-cfb,aes-192-cfb,chacha20,chacha20-ietf;aead cipher:aes-gcm,chacha20-poly1305")
	socks5ServerCmd.Flags().IntVarP(&socks5ServerConfig.PreferIpv6, "prefer-ipv6", "i", 1, "ipv6 first,0:disable,1:enable")

	socks5ServerCmd.Run = func(cmd *cobra.Command, args []string) {
		err := yaml.Unmarshal(confBytes, socks5ServerConfig)
		if err != nil {
			panic(err)
		}
		log.Printf("config from file:%+v", socks5ServerConfig)

		s, err := socks5.NewSocks5Server(socks5ServerConfig)
		if err != nil {
			log.Println(err)
			return
		}
		s.Serve()
	}
	socks5Cmd.AddCommand(socks5ServerCmd)
}
