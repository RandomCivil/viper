package main

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/internal"
	"gitlab.com/RandomCivil/viper/kcp"
	"gopkg.in/yaml.v2"
)

var kcpServerConfig = &config.KcpServerConfig{}

func init() {
	kcpServerCmd := &cobra.Command{Use: "server"}
	kcpServerCmd.Flags().StringVarP(&kcpServerConfig.Pwd, "pwd", "p", "foobar", "password")
	kcpServerCmd.Flags().IntVarP(&kcpServerConfig.Listen, "listen", "l", 12345, "client listening port")
	kcpServerCmd.Flags().IntVarP(&kcpServerConfig.DnsCleanInterval, "dns-clean-interval", "", internal.DNS_CACHE_EXPIRE, "dns clean interval")
	kcpServerCmd.Flags().IntVarP(&kcpServerConfig.KeepAliveInterval, "keep-alive-interval", "k", 10, "keep alive interval")
	kcpServerCmd.Flags().IntVarP(&kcpServerConfig.SndWnd, "send-window-size", "", 1024, "send window size")
	kcpServerCmd.Flags().IntVarP(&kcpServerConfig.RcvWnd, "receive-window-size", "", 1024, "recv window size")
	kcpServerCmd.Flags().StringVarP(&kcpServerConfig.Mode, "mode", "m", "fast", "mode:normal,fast,fast2,fast3")
	kcpServerCmd.Flags().IntVarP(&kcpServerConfig.PreferIpv6, "prefer-ipv6", "i", 1, "ipv6 first,0:disable,1:enable")

	kcpServerCmd.Run = func(cmd *cobra.Command, args []string) {
		err := yaml.Unmarshal(confBytes, kcpServerConfig)
		if err != nil {
			panic(err)
		}
		log.Printf("config from file:%+v", kcpServerConfig)

		kcpServerConfig.DealKcpMode()
		s := kcp.NewKcpServer(kcpServerConfig)
		s.Serve()
	}
	kcpCmd.AddCommand(kcpServerCmd)
}
