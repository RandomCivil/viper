package main

import (
	"io/ioutil"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/internal"
	"gitlab.com/RandomCivil/viper/quic"
	"gopkg.in/yaml.v2"
)

var quicClientConfig = &config.QuicClientConfig{}

func init() {
	quicClientCmd := &cobra.Command{Use: "client"}
	quicClientCmd.Flags().IntVarP(&quicClientConfig.Listen, "listen", "l", 2000, "server listening port")
	quicClientCmd.Flags().StringVarP(&quicClientConfig.ServerAddr, "server", "s", "", "server address")
	quicClientCmd.Flags().StringVarP(&quicClientConfig.AuthCertPath, "cert", "c", "", "client auth cert path")
	quicClientCmd.Flags().StringVarP(&quicClientConfig.AuthKeyPath, "key", "k", "", "client auth key path")
	quicClientCmd.Flags().StringVarP(&quicClientConfig.GfwListPath, "gfwlist-path", "", homePath+"/gfwlist.txt", "gfwlist path")
	quicClientCmd.Flags().StringVarP(&quicClientConfig.GeoIP2Path, "geoip2-path", "", homePath+"/GeoLite2-Country.mmdb", "geoip2 data path")
	quicClientCmd.Flags().IntVarP(&quicClientConfig.RouteStrategy, "route-strategy", "r", 0, "client route stategy,0:proxy all;1:proxy gfwlist;2:proxy gfwlist+forign ip")
	quicClientCmd.Flags().StringVarP(&quicClientConfig.ConnStrategy, "connection-strategy", "x", "lsf", "connection strategy:fixed,lsf,flex")
	quicClientCmd.Flags().IntVarP(&quicClientConfig.ConnNum, "connection-num", "n", 5, "quicp session num")
	quicClientCmd.Flags().IntVarP(&quicClientConfig.MaxStreamPerSession, "stream-num", "", 10, "max stream per session")
	quicClientCmd.Flags().IntVarP(&quicClientConfig.DnsCleanInterval, "dns-clean-interval", "", internal.DNS_CACHE_EXPIRE, "dns clean interval")

	quicClientCmd.Run = func(cmd *cobra.Command, args []string) {
		err := yaml.Unmarshal(confBytes, quicClientConfig)
		if err != nil {
			panic(err)
		}
		log.Printf("config from file:%+v", quicClientConfig)

		certPEMBlock, err := ioutil.ReadFile(quicClientConfig.AuthCertPath)
		if err != nil {
			panic(err)
		}
		keyPEMBlock, err := ioutil.ReadFile(quicClientConfig.AuthKeyPath)
		if err != nil {
			panic(err)
		}

		c, err := quic.NewQuicClient(quicClientConfig, certPEMBlock, keyPEMBlock)
		if err != nil {
			log.Println(err)
			return
		}
		c.Serve()
	}
	quicCmd.AddCommand(quicClientCmd)
}
