package main

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/internal"
	"gitlab.com/RandomCivil/viper/socks5"
	"gopkg.in/yaml.v2"
)

var socks5ClientConfig = &config.Socks5ClientConfig{}

func init() {
	socks5ClientCmd := &cobra.Command{Use: "client"}
	socks5ClientCmd.Flags().IntVarP(&socks5ClientConfig.Listen, "listen", "l", 2000, "client listening port")
	socks5ClientCmd.Flags().StringVarP(&socks5ClientConfig.Pwd, "pwd", "p", "foobar", "password")
	socks5ClientCmd.Flags().StringVarP(&socks5ClientConfig.Cipher, "cipher", "c", "aes-gcm", "cipher method,stream cipher:aes-128-cfb,aes-192-cfb,chacha20,chacha20-ietf;aead cipher:aes-gcm,chacha20-poly1305")
	socks5ClientCmd.Flags().StringVarP(&socks5ClientConfig.ServerAddr, "server", "s", "127.0.0.1:3000", "server address")
	socks5ClientCmd.Flags().IntVarP(&socks5ClientConfig.ConnNum, "connection-num", "n", 5, "num of connections")
	socks5ClientCmd.Flags().StringVarP(&socks5ClientConfig.ConnStrategy, "connection-strategy", "x", "flex", "connection strategy:fixed,lsf,flex")
	socks5ClientCmd.Flags().IntVarP(&socks5ClientConfig.MaxStreamPerSession, "stream-num", "", 10, "max stream per session")
	socks5ClientCmd.Flags().IntVarP(&socks5ClientConfig.DnsCleanInterval, "dns-clean-interval", "", internal.DNS_CACHE_EXPIRE, "dns clean interval")
	socks5ClientCmd.Flags().StringVarP(&socks5ClientConfig.GfwListPath, "gfwlist-path", "", homePath+"/gfwlist.txt", "gfwlist path")
	socks5ClientCmd.Flags().StringVarP(&socks5ClientConfig.GeoIP2Path, "geoip2-path", "", homePath+"/GeoLite2-Country.mmdb", "geoip2 data path")
	socks5ClientCmd.Flags().IntVarP(&socks5ClientConfig.RouteStrategy, "route-strategy", "r", 0, "client route stategy,0:proxy all;1:proxy gfwlist;2:proxy gfwlist+forign ip")

	socks5ClientCmd.Run = func(cmd *cobra.Command, args []string) {
		err := yaml.Unmarshal(confBytes, socks5ClientConfig)
		if err != nil {
			log.Println(err)
			return
		}
		log.Printf("config from file:%+v", socks5ClientConfig)

		socks5ClientConfig.SmuxKeepAliveTimeout = 10000
		socks5ClientConfig.SmuxKeepAliveInterval = 2000
		c, err := socks5.NewSocks5Client(socks5ClientConfig)
		if err != nil {
			log.Println(err)
			return
		}
		c.Serve()
	}

	socks5Cmd.AddCommand(socks5ClientCmd)
}
