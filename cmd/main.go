package main

import (
	"flag"
	"io/ioutil"
	"log"
	"os"

	"runtime/trace"

	"github.com/spf13/cobra"
	"gitlab.com/RandomCivil/common"
	logger "gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/common/profile"
)

var (
	openTrace int
	traceFile *os.File
	confPath  string
	confBytes []byte

	rootCmd   *cobra.Command
	socks5Cmd = &cobra.Command{Use: "socks5"}
	kcpCmd    = &cobra.Command{Use: "kcp"}
	quicCmd   = &cobra.Command{Use: "quic"}
	docCmd    = &cobra.Command{Use: "doc"}

	homePath = os.Getenv("HOME")
	version  = flag.Bool("version", false, "Show current version")
)

func init() {
	rootCmd = &cobra.Command{
		Use: "viper",
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			logger.New()
			if openTrace == 1 {
				log.Println("start trace")
				traceFile = profile.DoTrace()
			}

			if confPath != "" {
				var err error
				confBytes, err = ioutil.ReadFile(confPath)
				if err != nil {
					log.Fatalf("failed to read config file %s,err:%v", confPath, err)
				}
			}
		},
		PersistentPostRun: func(cmd *cobra.Command, args []string) {
			if openTrace == 1 {
				log.Println("stop trace")
				trace.Stop()
				if err := traceFile.Close(); err != nil {
					log.Fatalf("failed to close trace file: %v", err)
				}
			}
		},
	}
	rootCmd.AddCommand(socks5Cmd)
	rootCmd.AddCommand(kcpCmd)
	rootCmd.AddCommand(quicCmd)
	rootCmd.AddCommand(docCmd)

	rootCmd.PersistentFlags().IntVarP(&openTrace, "trace", "t", 0, "open trace,1:enable,0:disable")
	rootCmd.PersistentFlags().IntVarP(&logger.Level, "log", "", 3, "log level.1:error,2:info,3:debug")
	rootCmd.PersistentFlags().StringVarP(&confPath, "conf", "", "", "config file")
}

func main() {
	flag.Parse()
	common.Verbose()
	if *version {
		return
	}

	if err := rootCmd.Execute(); err != nil {
		log.Printf("root cmd execute err:%v", err)
		os.Exit(1)
	}
}
