package main

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/internal"
	"gitlab.com/RandomCivil/viper/kcp"
	"gopkg.in/yaml.v2"
)

var kcpClientConfig = &config.KcpClientConfig{}

func init() {
	kcpClientCmd := &cobra.Command{Use: "client"}
	kcpClientCmd.Flags().StringVarP(&kcpClientConfig.ServerAddr, "server", "s", "127.0.0.1:12345", "server address")
	kcpClientCmd.Flags().StringVarP(&kcpClientConfig.Pwd, "pwd", "p", "foobar", "password")
	kcpClientCmd.Flags().IntVarP(&kcpClientConfig.ConnNum, "connection-num", "n", 5, "kcp session num")
	kcpClientCmd.Flags().IntVarP(&kcpClientConfig.Listen, "listen", "l", 2000, "client listening port")
	kcpClientCmd.Flags().IntVarP(&kcpClientConfig.KeepAliveInterval, "keep-alive-interval", "k", 10, "keep alive interval")
	kcpClientCmd.Flags().IntVarP(&kcpClientConfig.SndWnd, "send-window-size", "", 1024, "send window size")
	kcpClientCmd.Flags().IntVarP(&kcpClientConfig.RcvWnd, "receive-window-size", "", 1024, "recv window size")
	kcpClientCmd.Flags().StringVarP(&kcpClientConfig.Mode, "mode", "m", "fast", "mode:normal,fast,fast2,fast3")
	kcpClientCmd.Flags().StringVarP(&kcpClientConfig.ConnStrategy, "connection-strategy", "x", "lsf", "connection strategy:fixed,lsf")
	kcpClientCmd.Flags().IntVarP(&kcpClientConfig.DnsCleanInterval, "dns-clean-interval", "", internal.DNS_CACHE_EXPIRE, "dns clean interval")
	kcpClientCmd.Flags().IntVarP(&kcpClientConfig.SmuxKeepAliveInterval, "si", "", 5000, "smux keep alive interval")
	kcpClientCmd.Flags().IntVarP(&kcpClientConfig.SmuxKeepAliveTimeout, "st", "", 10000, "smux keep alive timeout")
	kcpClientCmd.Flags().StringVarP(&kcpClientConfig.GfwListPath, "gfwlist-path", "", homePath+"/gfwlist.txt", "gfwlist path")
	kcpClientCmd.Flags().StringVarP(&kcpClientConfig.GeoIP2Path, "geoip2-path", "", homePath+"/GeoLite2-Country.mmdb", "geoip2 data path")
	kcpClientCmd.Flags().IntVarP(&kcpClientConfig.RouteStrategy, "route-strategy", "r", 0, "client route stategy,0:proxy all;1:proxy gfwlist;2:proxy gfwlist+forign ip")

	kcpClientCmd.Run = func(cmd *cobra.Command, args []string) {
		err := yaml.Unmarshal(confBytes, kcpClientConfig)
		if err != nil {
			panic(err)
		}
		log.Printf("config from file:%+v", kcpClientConfig)

		kcpClientConfig.DealKcpMode()
		c := kcp.NewKcpClient(kcpClientConfig)
		c.Serve()
	}
	kcpCmd.AddCommand(kcpClientCmd)
}
