package main

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
)

var (
	docDir string
)

func init() {
	docCmd.Flags().StringVarP(&docDir, "dir", "d", homePath+"/doc", "doc dir")
	docCmd.Run = func(cmd *cobra.Command, args []string) {
		err := os.Mkdir(docDir, 0755)
		if err != nil {
			log.Fatal(err)
		}

		err = doc.GenMarkdownTree(cmd.Root(), docDir)
		if err != nil {
			log.Fatal(err)
		}
	}
}
