package main

import (
	"io/ioutil"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/internal"
	"gitlab.com/RandomCivil/viper/quic"
	"gopkg.in/yaml.v2"
)

var quicServerConfig = &config.QuicServerConfig{}

func init() {
	quicServerCmd := &cobra.Command{Use: "server"}
	quicServerCmd.Flags().IntVarP(&quicServerConfig.Listen, "listen", "l", 8443, "server listening port")
	quicServerCmd.Flags().IntVarP(&quicServerConfig.PreferIpv6, "prefer-ipv6", "i", 1, "ipv6 first,0:disable,1:enable")
	quicServerCmd.Flags().IntVarP(&quicServerConfig.DnsCleanInterval, "dns-clean-interval", "", internal.DNS_CACHE_EXPIRE, "dns clean interval")
	quicServerCmd.Flags().StringVarP(&quicServerConfig.CertPath, "cert", "c", "", "cert path")
	quicServerCmd.Flags().StringVarP(&quicServerConfig.KeyPath, "key", "k", "", "key path")
	quicServerCmd.Flags().StringVarP(&quicServerConfig.AuthCertPath, "auth", "a", "", "client auth cert path")

	quicServerCmd.Run = func(cmd *cobra.Command, args []string) {
		err := yaml.Unmarshal(confBytes, quicServerConfig)
		if err != nil {
			panic(err)
		}
		log.Printf("config from file:%+v", quicServerConfig)

		certPEMBlock, err := ioutil.ReadFile(quicServerConfig.CertPath)
		if err != nil {
			panic(err)
		}
		keyPEMBlock, err := ioutil.ReadFile(quicServerConfig.KeyPath)
		if err != nil {
			panic(err)
		}
		authCertBytes, err := ioutil.ReadFile(quicServerConfig.AuthCertPath)
		if err != nil {
			panic("Unable to read cert.pem")
		}

		s := quic.NewQuicServer(quicServerConfig)
		s.Serve(certPEMBlock, keyPEMBlock, authCertBytes)
	}
	quicCmd.AddCommand(quicServerCmd)
}
