package config

type Socks5ServerConfig struct {
	Listen           int
	Pwd              string
	Cipher           string
	PreferIpv6       int `yaml:"prefer-ipv6"`
	DnsCleanInterval int `yaml:"dns-clean-interval"`
}

type Socks5ClientConfig struct {
	ServerAddr          string `yaml:"server"`
	Listen              int
	ConnNum             int `yaml:"connection-num"`
	Pwd                 string
	Cipher              string
	ConnStrategy        string `yaml:"connection-strategy"`
	MaxStreamPerSession int    `yaml:"stream-num"`
	DnsCleanInterval    int    `yaml:"dns-clean-interval"`
	SmuxConfig
	RouteConfig `yaml:",inline"`
}

type SmuxConfig struct {
	SmuxKeepAliveInterval, SmuxKeepAliveTimeout int
}

type RouteConfig struct {
	GfwListPath   string `yaml:"gfwlist-path"`
	GeoIP2Path    string `yaml:"geoip2-path"`
	RouteStrategy int    `yaml:"route-strategy"`
}

func (c *RouteConfig) GetGfwListPath() string {
	return c.GfwListPath
}

func (c *RouteConfig) GetGeoIP2Path() string {
	return c.GeoIP2Path
}

func (c *RouteConfig) GetRouteStrategy() int {
	return c.RouteStrategy
}
