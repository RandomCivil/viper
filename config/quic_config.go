package config

type QuicServerConfig struct {
	Listen           int
	PreferIpv6       int    `yaml:"prefer-ipv6"`
	CertPath         string `yaml:"cert"`
	KeyPath          string `yaml:"key"`
	AuthCertPath     string `yaml:"auth"`
	DnsCleanInterval int    `yaml:"dns-clean-interval"`
}

type QuicClientConfig struct {
	ServerAddr          string `yaml:"server"`
	Listen              int
	AuthCertPath        string `yaml:"cert"`
	AuthKeyPath         string `yaml:"key"`
	RouteConfig         `yaml:",inline"`
	ConnStrategy        string `yaml:"connection-strategy"`
	ConnNum             int    `yaml:"connection-num"`
	MaxStreamPerSession int    `yaml:"stream-num"`
	DnsCleanInterval    int    `yaml:"dns-clean-interval"`
}
