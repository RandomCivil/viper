package config

type KcpClientConfig struct {
	ServerAddr          string `yaml:"server"`
	Pwd                 string
	ConnNum             int `yaml:"connection-num"`
	Listen              int
	KeepAliveInterval   int    `yaml:"keep-alive-interval"`
	SndWnd              int    `yaml:"send-window-size"`
	RcvWnd              int    `yaml:"receive-window-size"`
	ConnStrategy        string `yaml:"connection-strategy"`
	MaxStreamPerSession int    `yaml:"stream-num"`
	DnsCleanInterval    int    `yaml:"dns-clean-interval"`
	KcpModeConfig
	SmuxConfig
	RouteConfig `yaml:",inline"`
}

type KcpModeConfig struct {
	Mode                                    string
	NoDelay, Interval, Resend, NoCongestion int
}

func (c *KcpModeConfig) DealKcpMode() {
	switch c.Mode {
	case "normal":
		c.NoDelay, c.Interval, c.Resend, c.NoCongestion = 0, 40, 2, 1
	case "fast":
		c.NoDelay, c.Interval, c.Resend, c.NoCongestion = 0, 30, 2, 1
	case "fast2":
		c.NoDelay, c.Interval, c.Resend, c.NoCongestion = 1, 20, 2, 1
	case "fast3":
		c.NoDelay, c.Interval, c.Resend, c.NoCongestion = 1, 10, 2, 1
	}
}

type KcpServerConfig struct {
	Pwd               string
	Listen            int
	KeepAliveInterval int `yaml:"keep-alive-interval"`
	SndWnd            int `yaml:"send-window-size"`
	RcvWnd            int `yaml:"receive-window-size"`
	PreferIpv6        int `yaml:"prefer-ipv6"`
	DnsCleanInterval  int `yaml:"dns-clean-interval"`
	KcpModeConfig
}
