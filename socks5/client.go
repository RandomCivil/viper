package socks5

import (
	"context"
	"io"
	"net"
	"runtime"
	"strconv"
	"time"

	"gitlab.com/RandomCivil/common/log"
	sn "gitlab.com/RandomCivil/common/signal"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/encrypt"
	"gitlab.com/RandomCivil/viper/network"
)

type socks5Client struct {
	signal     *sn.Signal
	dialFunc   func(ctx context.Context, i int32) *network.DialTarget
	sa         *network.ServerAddr
	config     *config.Socks5ClientConfig
	se         *network.Session
	Ln         net.Listener
	done       chan struct{}
	cipherType string
	dc         *network.DnsCache
	router     *Router
}

func NewSocks5Client(conf *config.Socks5ClientConfig) (*socks5Client, error) {
	r := NewRouter(conf)

	dc := network.NewDnsCache(conf.DnsCleanInterval, conf.DnsCleanInterval)
	dc.SetDnsPriority(false)

	client := &socks5Client{
		signal: sn.NewSignal(),
		config: conf,
		done:   make(chan struct{}, 1),
		dc:     dc,
		router: r,
	}

	cipherType, err := encrypt.CheckParam(conf.Cipher, conf.Pwd)
	if err != nil {
		return nil, err
	}
	client.cipherType = cipherType

	host, _, err := net.SplitHostPort(conf.ServerAddr)
	if err != nil {
		panic(err)
	}

	var ipType string
	ip := net.ParseIP(host)
	if ip.To4() != nil {
		ipType = "tcp"
	} else if ip.To16() != nil {
		ipType = "tcp6"
	}

	serverAddr := &network.ServerAddr{
		Addr:   conf.ServerAddr,
		IpType: ipType,
	}
	client.sa = serverAddr

	sessionConfig := &network.SessionConfig{
		Strategy:            conf.ConnStrategy,
		Num:                 conf.ConnNum,
		MaxStreamPerSession: conf.MaxStreamPerSession,
	}
	dialFunc := func(ctx context.Context, i int32) *network.DialTarget {
		dialer := net.Dialer{Timeout: time.Second * 2}
		target, err := dialer.DialContext(ctx, serverAddr.IpType, serverAddr.Addr)
		r := new(network.DialTarget)
		r.Target = target
		if err == nil {
			r.Done = true
			r.Conf = &config.SmuxConfig{
				SmuxKeepAliveInterval: conf.SmuxKeepAliveInterval,
				SmuxKeepAliveTimeout:  conf.SmuxKeepAliveTimeout,
			}
		}
		return r
	}
	client.dialFunc = dialFunc
	session, err := network.NewSession(sessionConfig, dialFunc)
	if err != nil {
		return nil, err
	}
	client.se = session

	return client, nil
}

func (c *socks5Client) clientConn(netConn net.Conn) {
	netConn.SetDeadline(time.Now().Add(60 * time.Second))
	defer netConn.Close()

	rc := &RouteConn{
		NetConn:   netConn,
		Strategy:  c.config.RouteStrategy,
		Router:    c.router,
		DnsCache:  c.dc,
		ReturnCh:  make(chan struct{}, 1),
		PipeCh:    make(chan *PipeConn, 1),
		BufConnCh: make(chan io.Reader, 1),
	}
	var concreteConn io.Reader
	HandleClientConn(rc)
	select {
	case <-rc.ReturnCh:
		return
	case pipeConn := <-rc.PipeCh:
		concreteConn = pipeConn.Pr
		defer pipeConn.Pw.Close()
		defer pipeConn.Pr.Close()
	case bufConn := <-rc.BufConnCh:
		concreteConn = bufConn
	}

	session := c.se.Fetch()
	//create new session fail when flex fetch
	if session == nil {
		return
	}
	stream, err := session.OpenStream()
	//session closed
	if err != nil {
		log.DefaultLogger.Errorf("open stream err:%v", err)
		session, stream = c.se.MakeStream(session, c.config.ConnStrategy)
	}

	c.se.Ch <- session
	defer session.CloseStream(stream)

	pipe := newPipe()
	pipe.needSendTs = true
	errCh := make(chan *pipeErr, 2)
	go func() {
		encCipher := NewEncCipher(c.config.Cipher, c.config.Pwd, c.cipherType)
		pipe.enc(stream, concreteConn, encCipher, errCh)
		encCipher.Release()
	}()
	go func() {
		decCipher := NewDecCipher(stream, c.config.Cipher, c.config.Pwd, c.cipherType)
		pipe.dec(netConn, stream, decCipher, errCh)
		decCipher.Release()
	}()
	pipeErr := <-errCh
	log.DefaultLogger.Infof("stream in session %d pipe err %+v", session.GetIndex(), pipeErr)
}

func (c *socks5Client) Serve() {
	runtime.GC()
	l, err := net.Listen("tcp", ":"+strconv.Itoa(c.config.Listen))
	if err != nil {
		log.DefaultLogger.Errorf("listen err:%v", err)
		return
	}
	c.Ln = l
	c.done <- struct{}{}
	log.DefaultLogger.Infof("client start at %s", l.Addr().String())

	defer l.Close()
	defer c.se.Close()

	go c.signal.Handle(l)
	for {
		conn, err := l.Accept()
		if err != nil {
			log.DefaultLogger.Errorf("accept error:%v", err)
			return
		}
		go c.clientConn(conn)
	}
}
