package socks5

import (
	"bytes"
	"context"
	"crypto/rand"
	"io"
	"net"
	"os"
	"testing"
	"time"

	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/network"
)

func TestMain(m *testing.M) {
	ms := &viper.MockServer{
		StopCh: make(chan struct{}, 1),
		DoneCh: make(chan struct{}, 1),
	}
	go ms.Start(8443, 8079)
	<-ms.DoneCh

	r := m.Run()
	os.Exit(r)
	ms.Stop()
}

func makeBothStart(t *testing.T, sc *config.Socks5ServerConfig, cc *config.Socks5ClientConfig) (*socks5Server, *socks5Client) {
	BlockIps.Delete("127.0.0.1")
	BlockIps.Delete("::1")
	done := make(chan struct{})
	ch := make(chan struct{})
	server, err := NewSocks5Server(sc)
	if err != nil {
		panic(err)
	}
	var client *socks5Client

	go func() {
		server.Serve()
	}()
	go func() {
		<-server.done
		t.Log("server start", server.ln.Addr().String())
		cc.ServerAddr = server.ln.Addr().String()
		client, _ = NewSocks5Client(cc)
		ch <- struct{}{}
		client.Serve()
	}()
	go func() {
		<-ch
		<-client.done
		done <- struct{}{}
	}()
	<-done
	t.Log("client start")
	return server, client
}

func TestReplay(t *testing.T) {
	server, err := startServer(newServerDefaultConf(), t)
	if err != nil {
		t.Fatal(err)
	}
	defer server.ln.Close()

	dialer := net.Dialer{Timeout: time.Second * 2}
	target, err := dialer.DialContext(context.Background(), "tcp", server.ln.Addr().String())
	if err != nil {
		t.Fatal(err)
	}

	iv := make([]byte, 16)
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		t.Fatal(err)
	}

	//smux syn
	target.Write([]byte{1, 0, 0, 0, 3, 0, 0, 0})
	buf := bytes.NewBuffer(nil)
	//smux psh
	buf.Write([]byte{1, 2, 62, 0, 3, 0, 0, 0})
	//iv
	buf.Write([]byte{54, 106, 78, 53, 95, 37, 192, 72, 57, 224, 209, 112, 57, 239, 42, 138})
	buf.Write([]byte{255, 176, 211, 33, 7, 166, 65, 88, 62, 10, 210, 211, 68, 96, 137, 87, 11, 8, 123, 138, 74, 44, 195, 54, 64, 203, 127, 28, 150, 118, 52, 185, 151, 143, 211, 129, 101, 220, 153, 114, 230, 163, 129, 175, 249, 232})
	t.Log(buf.Bytes())

	target.Write(buf.Bytes())
	target.SetDeadline(time.Now().Add(15 * time.Second))

	b := make([]byte, 100)
	n, err := io.ReadFull(target, b)
	t.Log(n, err, b[:n])
	if err == nil {
		t.Fatal("TestReplay fail")
	}
}

func startClient(clientConfig *config.Socks5ClientConfig, t *testing.T) (*socks5Client, error) {
	client, err := NewSocks5Client(clientConfig)
	if err != nil {
		return nil, err
	}
	go func() {
		client.Serve()
	}()
	<-client.done
	t.Log("client started")
	return client, nil
}

func startServer(serverConfig *config.Socks5ServerConfig, t *testing.T) (*socks5Server, error) {
	serverConfig.DnsCleanInterval = 60
	server, err := NewSocks5Server(serverConfig)
	if err != nil {
		return nil, err
	}
	go func() {
		server.Serve()
	}()
	<-server.done
	t.Log("server started")
	return server, nil
}

func newServerDefaultConf() *config.Socks5ServerConfig {
	c := new(config.Socks5ServerConfig)
	c.Listen = 0
	c.Pwd = "foobar"
	c.Cipher = "aes-gcm"
	c.PreferIpv6 = 0
	c.DnsCleanInterval = 60
	return c
}

func newClientDefaultConf() *config.Socks5ClientConfig {
	c := new(config.Socks5ClientConfig)
	c.Listen = 0
	c.Pwd = "foobar"
	c.ConnStrategy = "lsf"
	c.ConnNum = 1
	c.Cipher = "aes-gcm"
	c.SmuxKeepAliveTimeout = 10000
	c.SmuxKeepAliveInterval = 2000
	c.RouteStrategy = 0
	c.DnsCleanInterval = 60

	c.GeoIP2Path = viper.TestRootPath + "/GeoLite2-Country.mmdb"
	c.GfwListPath = viper.TestRootPath + "/gfwlist.txt"
	return c
}

func TestRouteStrategy(t *testing.T) {
	tests := []struct {
		reqUrl   string
		strategy int
	}{
		{"https://www.baidu.com", 1},
		{"https://www.amazon.com", 1},
		{"https://www.baidu.com", 2},
		{"https://www.amazon.com", 2},
	}
	sc := newServerDefaultConf()
	cc := newClientDefaultConf()

	for _, tt := range tests {
		cc.RouteStrategy = tt.strategy
		server, client := makeBothStart(t, sc, cc)
		resp, err := viper.RequestViaTransport(t, client.Ln.Addr().String(), tt.reqUrl)
		if err != nil {
			t.Fatal("TestRouteStrategy fail", err)
		}
		t.Log(resp.StatusCode, err)
		t.Log(resp.Proto)
		resp.Body.Close()
		time.Sleep(200 * time.Millisecond)
		client.Ln.Close()
		server.ln.Close()
	}
}

func TestSuccessAuth(t *testing.T) {
	tests := []struct {
		reqUrl, cipher string
	}{
		{"https://localhost:8443", "aes-gcm"},
		{"https://127.0.0.1:8443", "chacha20"},
		{"http://localhost:8079", "chacha20"},
		{"http://127.0.0.1:8079", "aes-gcm"},
		{"https://[::1]:8443", "aes-gcm"},
		{"http://[::1]:8079", "aes-gcm"},
		{"https://www.baidu.com", "aes-gcm"},
		{"https://www.baidu.com", "chacha20"},
	}
	sc := newServerDefaultConf()
	cc := newClientDefaultConf()

	for _, tt := range tests {
		sc.Cipher = tt.cipher
		cc.Cipher = tt.cipher
		server, client := makeBothStart(t, sc, cc)
		resp, err := viper.RequestViaTransport(t, client.Ln.Addr().String(), tt.reqUrl)
		if err != nil {
			t.Fatal("TestSuccessAuth fail", err)
		}
		t.Log(resp.StatusCode, err)
		t.Log(resp.Proto)
		resp.Body.Close()
		client.Ln.Close()
		server.ln.Close()
	}
}

func TestFailCipherPwd(t *testing.T) {
	server, _ := startServer(newServerDefaultConf(), t)
	defer server.ln.Close()

	tests := []struct {
		reqUrl, pwd string
	}{
		{"https://localhost:8443", "123"},
		{"http://localhost:8079", "123"},
	}
	for _, tt := range tests {
		config := newClientDefaultConf()
		config.Pwd = tt.pwd
		config.ServerAddr = server.ln.Addr().String()
		client, _ := startClient(config, t)

		resp, err := viper.RequestViaTransport(t, client.Ln.Addr().String(), tt.reqUrl)
		t.Log(resp, err)
		if err == nil {
			t.Fatal("TestFailCipherPwd", err)
		}
		client.Ln.Close()
	}
	BlockIps.Delete("127.0.0.1")
	BlockIps.Delete("::1")
}

func TestBlockIp(t *testing.T) {
	server, _ := startServer(newServerDefaultConf(), t)
	defer server.ln.Close()

	tests := []struct {
		reqUrl, pwd string
	}{
		{"https://localhost:8443", "123"},
		{"https://localhost:8443", "foobar"},
		{"https://localhost:8443", "124"},
	}
	for _, tt := range tests {
		config := newClientDefaultConf()
		config.ConnStrategy = "flex"
		config.Pwd = tt.pwd
		config.ServerAddr = server.ln.Addr().String()
		client, _ := startClient(config, t)

		resp, err := viper.RequestViaTransport(t, client.Ln.Addr().String(), tt.reqUrl)
		t.Log(resp, err)
		if err == nil {
			t.Fatal("TestBlockIp", err)
		}
		client.Ln.Close()
	}
	BlockIps.Delete("127.0.0.1")
	BlockIps.Delete("::1")
}

func TestMakeStream(t *testing.T) {
	server, _ := startServer(newServerDefaultConf(), t)
	defer server.ln.Close()

	tests := []struct {
		reqUrl, strategy string
	}{
		{"https://localhost:8443", "lsf"},
		{"https://localhost:8443", "flex"},
		{"https://localhost:8443", "fixed"},
	}

	cc := newClientDefaultConf()
	cc.SmuxKeepAliveTimeout = 1000
	cc.SmuxKeepAliveInterval = 200
	for _, tt := range tests {
		cc.ServerAddr = server.ln.Addr().String()
		cc.ConnNum = 1
		cc.ConnStrategy = tt.strategy
		client, _ := startClient(cc, t)
		for i := 0; i < 2; i++ {
			resp, err := viper.RequestViaTransport(t, client.Ln.Addr().String(), tt.reqUrl)
			if err != nil {
				t.Fatal("TestSuccessAuth", err)
			}
			t.Log(resp.StatusCode, err)
			t.Log(resp.Proto)
			resp.Body.Close()
			time.Sleep(2 * time.Second)
		}
		client.Ln.Close()
	}
}

func TestIpv6First(t *testing.T) {
	tests := []struct {
		reqUrl string
	}{
		{"https://localhost:8443"},
		{"https://127.0.0.1:8443"},
		{"http://localhost:8079"},
		{"http://127.0.0.1:8079"},
	}

	sc := newServerDefaultConf()
	sc.PreferIpv6 = 1
	server, client := makeBothStart(t, sc, newClientDefaultConf())
	defer client.Ln.Close()
	defer server.ln.Close()

	for _, tt := range tests {
		resp, err := viper.RequestViaTransport(t, client.Ln.Addr().String(), tt.reqUrl)
		if err != nil {
			t.Fatal("TestIpv6First", err)
		}
		t.Log(resp.StatusCode, err)
		t.Log(resp.Proto)
		resp.Body.Close()
	}
}

func TestWrongDomain(t *testing.T) {
	server, _ := startServer(newServerDefaultConf(), t)
	defer server.ln.Close()

	strategys := []int{0, 2}
	cc := newClientDefaultConf()
	cc.ServerAddr = server.ln.Addr().String()
	for _, s := range strategys {
		cc.RouteStrategy = s
		client, _ := startClient(cc, t)

		resp, err := viper.RequestViaTransport(t, client.Ln.Addr().String(), "https://afjogjowgw.com")
		t.Log(err)
		if err == nil {
			t.Fatalf("TestWrongDomain error,err:%v,resp:%v", err, resp)
		}

		client.Ln.Close()
	}
}

func TestWrongEncrypt(t *testing.T) {
	sc := newServerDefaultConf()
	sc.Cipher = "cfb-128"
	_, err := startServer(sc, t)
	if err == nil {
		t.Fatal("TestWrongEncrypt error")
	}

	cc := newClientDefaultConf()
	cc.Pwd = ""
	_, err = startClient(cc, t)
	if err == nil {
		t.Fatal("TestWrongEncrypt error")
	}
}

func TestWrongAddr(t *testing.T) {
	server, client := makeBothStart(t, newServerDefaultConf(), newClientDefaultConf())
	defer server.ln.Close()
	defer client.Ln.Close()

	resp, err := viper.RequestViaTransport(t, client.Ln.Addr().String(), "https://127.0.0.2:12345")
	t.Log(err)
	if err == nil {
		t.Fatalf("TestWrongAddr error,err:%v,resp:%v", err, resp)
	}
}

func TestWrongTarget(t *testing.T) {
	tests := []struct {
		strategy string
	}{
		{"flex"},
		{"lsf"},
		{"fixed"},
	}
	cc := newClientDefaultConf()
	cc.ConnNum = 3
	cc.ServerAddr = "127.0.0.1:12345"

	for _, tt := range tests {
		cc.ConnStrategy = tt.strategy
		client, err := NewSocks5Client(cc)
		if err == nil {
			t.Fatal("TestWrongTarget err")
		}
		t.Log(client, err)
	}
}

func TestInvalidAddrType(t *testing.T) {
	server, client := makeBothStart(t, newServerDefaultConf(), newClientDefaultConf())
	defer client.Ln.Close()
	defer server.ln.Close()

	target, err := net.Dial("tcp", client.Ln.Addr().String())
	if err != nil {
		t.Fatal(err)
	}

	target.Write([]byte{5, 2, 0, 2})

	recv := make([]byte, 2)
	io.ReadAtLeast(target, recv, 2)
	t.Log("auth method", recv)

	target.Write([]byte{5, 1, 0, 5, 14, 215, 177, 39, 80, 0})

	b := make([]byte, 100)
	n, err := io.ReadFull(target, b)
	t.Log(n, err, b[:n])
	if err == nil {
		t.Fatal("TestInvalidAddrType fail")
	}
	BlockIps.Delete("127.0.0.1")
	BlockIps.Delete("::1")
}

func TestDnsCache(t *testing.T) {
	dnsCache := network.NewDnsCache(5, 3)
	dnsCache.SetDnsPriority(true)

	var (
		err error
	)
	req := &Request{domain: []byte{119, 119, 119, 46, 98, 97, 105, 100, 117, 46, 99, 111, 109}}
	for i := 0; i < 3; i++ {
		err = handleDnsCache(dnsCache, req)
		if err != nil {
			t.Fatal(err)
		}
	}

	time.Sleep(5 * time.Second)
	dnsCache.SetDnsPriority(false)
	err = handleDnsCache(dnsCache, req)
	if err != nil {
		t.Fatal(err)
	}
}
