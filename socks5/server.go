package socks5

import (
	"bytes"
	"errors"
	"io"
	"net"
	"strconv"
	"sync"
	"time"

	"github.com/xtaci/smux"
	"gitlab.com/RandomCivil/common/log"
	sn "gitlab.com/RandomCivil/common/signal"
	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/encrypt"
	"gitlab.com/RandomCivil/viper/internal"
	"gitlab.com/RandomCivil/viper/network"
)

var (
	BlockIps   = sync.Map{}
	ErrInvalid = errors.New("invalid ip")
)

type socks5Server struct {
	signal     *sn.Signal
	config     *config.Socks5ServerConfig
	ln         net.Listener
	done       chan struct{}
	cipherType string
	dc         *network.DnsCache
}

type Session struct {
	CipherName string
	Pwd        string
	CipherType string
	Sm         *network.SessMap
	//socks5,kcp,quic
	Se       viper.SessionInterface
	DnsCache *network.DnsCache
}

func NewSocks5Server(config *config.Socks5ServerConfig) (*socks5Server, error) {
	dc := network.NewDnsCache(config.DnsCleanInterval, config.DnsCleanInterval)
	dc.SetDnsPriority(config.PreferIpv6 == 1)

	server := &socks5Server{
		signal: sn.NewSignal(),
		config: config,
		done:   make(chan struct{}, 1),
		dc:     dc,
	}

	cipherType, err := encrypt.CheckParam(config.Cipher, config.Pwd)
	if err != nil {
		return nil, err
	}
	server.cipherType = cipherType
	return server, nil
}

type Stream struct {
	stream     io.ReadWriteCloser
	cipher     string
	pwd        string
	cipherType string
	dnsCache   *network.DnsCache
}

type Streamer interface {
	Handle(s *Stream) error
}

type EncryptStream struct{}

func (es *EncryptStream) Handle(s *Stream) error {
	defer s.stream.Close()
	decCipher := NewDecCipher(s.stream, s.cipher, s.pwd, s.cipherType)

	req, err := NewRequest(s.stream, decCipher)
	switch err {
	case encrypt.ErrAeadDec, ErrReplay:
		log.DefaultLogger.Errorf("handle stream err:%v", err)
		return ErrInvalid
	case ErrInvalidAddrType:
		return nil
	}
	if req.typo == internal.DOMAIN_TYPE {
		err := handleDnsCache(s.dnsCache, req)
		if err == network.ErrDomainNotParsed {
			return nil
		}
	}

	target, err := HandleRequest(req)
	if err != nil {
		return nil
	}
	target.SetDeadline(time.Now().Add(30 * time.Second))
	defer target.Close()

	pipe := newPipe()
	encCipher := NewEncCipher(s.cipher, s.pwd, s.cipherType)

	//connect target success,send reply
	pipe.enc(s.stream, bytes.NewReader([]byte{5, 0, 0, 1, 0, 0, 0, 0, 0, 0}), encCipher, make(chan *pipeErr, 1))

	errCh := make(chan *pipeErr, 2)
	go func() {
		pipe.dec(target, s.stream, decCipher, errCh)
		decCipher.Release()
	}()
	go func() {
		pipe.enc(s.stream, target, encCipher, errCh)
		encCipher.Release()
	}()
	<-errCh
	return nil
}

type PlainStream struct{}

func (ps *PlainStream) Handle(s *Stream) error {
	defer s.stream.Close()
	req, err := NewRequest(s.stream, nil)
	switch err {
	case ErrInvalidAddrType:
		return nil
	}
	if req.typo == internal.DOMAIN_TYPE {
		err := handleDnsCache(s.dnsCache, req)
		if err == network.ErrDomainNotParsed {
			return nil
		}
	}

	target, err := HandleRequest(req)
	if err != nil {
		return nil
	}
	target.SetDeadline(time.Now().Add(30 * time.Second))
	defer target.Close()

	//connect target success,send reply
	s.stream.Write([]byte{5, 0, 0, 1, 0, 0, 0, 0, 0, 0})

	errCh := make(chan error, 2)
	go network.Pipe(target, s.stream, errCh)
	go network.Pipe(s.stream, target, errCh)
	err = <-errCh
	if v, ok := s.stream.(*smux.Stream); ok {
		log.DefaultLogger.Infof("stream %d pipe err %v", v.ID(), err)
	}
	return nil
}

func (s *socks5Server) handleConn(conn net.Conn, sm *network.SessMap) {
	ip := network.RemoteIP(conn)
	if _, ok := BlockIps.Load(ip); ok {
		log.DefaultLogger.Errorf("block ip %s", ip)
		conn.Close()
		return
	}

	session, _ := smux.Server(conn, nil)
	smuxSess := &network.SmuxSession{Se: session}
	wrapSess := &Session{
		Se:         smuxSess,
		CipherName: s.config.Cipher,
		Pwd:        s.config.Pwd,
		Sm:         sm,
		CipherType: s.cipherType,
		DnsCache:   s.dc,
	}
	sm.Set(ip, smuxSess, internal.SESSION_CLEAN_INTERVAL)
	HandleServerConn(wrapSess, &EncryptStream{})

	if wrapSess.Sm != nil {
		wrapSess.Sm.Del(ip, smuxSess)
	}
}

func (s *socks5Server) Serve() {
	l, err := net.Listen("tcp", ":"+strconv.Itoa(s.config.Listen))
	if err != nil {
		log.DefaultLogger.Errorf("listen err:%v", err)
		return
	}
	s.ln = l
	s.done <- struct{}{}
	log.DefaultLogger.Infof("server start at %s", l.Addr().String())

	defer l.Close()

	sessMap := network.NewSessMap()

	go s.signal.Handle(l)
	for {
		conn, err := l.Accept()
		if err != nil {
			log.DefaultLogger.Errorf("accept err:%v", err)
			return
		}
		go s.handleConn(conn, sessMap)
	}
}
