package socks5

import (
	"io"
	"strconv"
	"sync"
	"time"

	"gitlab.com/RandomCivil/common/bytespool"
	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/internal"
)

const (
	bufSize = 1024 * 32
)

type pipeErr struct {
	readErr, writeErr error
}

func (e *pipeErr) hasErr() bool {
	return e.readErr != nil || e.writeErr != nil
}

type pipe struct {
	sendIvOnce sync.Once
	needSendIv bool
	needSendTs bool
}

func newPipe() *pipe {
	return &pipe{}
}

func (p *pipe) dec(dst io.Writer, src io.Reader, cipher viper.Cipher, errCh chan *pipeErr) {
	ioErr := &pipeErr{}
	readBuf := bytespool.Alloc(bufSize)
	defer bytespool.Free(readBuf)
	for {
		nr, er := src.Read(readBuf)
		if nr > 0 {
			log.DefaultLogger.Debugf("dec before:%v", readBuf[:nr])
			nr, er = cipher.Decrypt(readBuf, readBuf[:nr])
			log.DefaultLogger.Debugf("dec after:%v,err:%v", readBuf[:nr], er)
			if er != nil {
				ioErr.readErr = er
				errCh <- ioErr
				break
			}
			nw, ew := dst.Write(readBuf[:nr])
			if nr != nw {
				ew = io.ErrShortWrite
			}
			if ew != nil {
				ioErr.writeErr = ew
			}
		}

		if er != nil {
			ioErr.readErr = er
		}
		if ioErr.hasErr() {
			errCh <- ioErr
			break
		}
	}
}

func (p *pipe) enc(dst io.Writer, src io.Reader, cipher viper.Cipher, errCh chan *pipeErr) {
	ioErr := &pipeErr{}
	writeBuf := bytespool.Alloc(bufSize)
	readBuf := writeBuf[:bufSize-internal.MAX_OVERHEAD]
	defer bytespool.Free(writeBuf)

	for {
		nr, er := src.Read(readBuf)
		if nr > 0 {
			var (
				nw    int
				ew    error
				ivLen = cipher.IVLen()
			)
			p.sendIvOnce.Do(func() {
				p.needSendIv = true
			})

			if p.needSendIv {
				//only client send ts
				if p.needSendTs {
					ts := strconv.Itoa(int(time.Now().Unix()))
					copy(readBuf[internal.TS_LEN:], readBuf[:nr])
					copy(readBuf[:internal.TS_LEN], []byte(ts))
					nr += internal.TS_LEN
					p.needSendTs = false
				}
				log.DefaultLogger.Debugf("enc before:%v", readBuf[:nr])
				nr = cipher.Encrypt(writeBuf, readBuf[:nr])
				log.DefaultLogger.Debugf("enc after:%v", writeBuf[:nr])

				copy(writeBuf[ivLen:], writeBuf[:nr])
				copy(writeBuf[:ivLen], cipher.IV())
				nw, ew = dst.Write(writeBuf[:nr+ivLen])
				nw -= ivLen
				p.needSendIv = false
			} else {
				log.DefaultLogger.Debugf("enc before:%v", readBuf[:nr])
				nr = cipher.Encrypt(writeBuf, readBuf[:nr])
				log.DefaultLogger.Debugf("enc after:%v", writeBuf[:nr])
				nw, ew = dst.Write(writeBuf[:nr])
			}

			if nr != nw {
				ew = io.ErrShortWrite
			}
			if ew != nil {
				ioErr.writeErr = ew
			}
		}
		if er != nil {
			ioErr.readErr = er
		}
		if ioErr.hasErr() {
			errCh <- ioErr
			break
		}
	}
}
