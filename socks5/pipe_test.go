package socks5

//go:generate go run github.com/golang/mock/mockgen -destination ../mock_writer.go -package viper gitlab.com/RandomCivil/viper Writer
//go:generate go run github.com/golang/mock/mockgen -destination ../mock_reader.go -package viper gitlab.com/RandomCivil/viper Reader
//go:generate go run github.com/golang/mock/mockgen -destination ../mock_cipher.go -package viper gitlab.com/RandomCivil/viper Cipher

import (
	"bytes"
	"io"
	"testing"

	gomock "github.com/golang/mock/gomock"
	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/encrypt"
)

func TestDecFail(t *testing.T) {
	pipe := newPipe()
	mockCtl := gomock.NewController(t)
	mockWriter := viper.NewMockWriter(mockCtl)
	mockWriter.EXPECT().Write(gomock.Any()).Return(100, nil).AnyTimes()

	mockReader := viper.NewMockReader(mockCtl)
	mockReader.EXPECT().Read(gomock.Any()).Return(100, nil)

	mockCipher := viper.NewMockCipher(mockCtl)
	mockCipher.EXPECT().Decrypt(gomock.Any(), gomock.Any()).Return(0, encrypt.ErrAeadDec).AnyTimes()

	errCh := make(chan *pipeErr, 2)
	pipe.dec(mockWriter, mockReader, mockCipher, errCh)
	err := <-errCh
	t.Log(err.readErr, err.writeErr)
	if !err.hasErr() || err.readErr != encrypt.ErrAeadDec {
		t.Fatalf("TestDecFail err,readErr:%v,writeErr:%v\n", err.readErr, err.writeErr)
	}
}

type writerReturn struct {
	n   int
	err error
}

func TestPipeWriterErr(t *testing.T) {
	pipe := newPipe()
	mockCtl := gomock.NewController(t)
	tests := []struct {
		wr       writerReturn
		expected error
	}{
		{writerReturn{100, io.ErrClosedPipe}, io.ErrClosedPipe},
		{writerReturn{10, io.EOF}, io.ErrShortWrite},
	}
	for _, tt := range tests {
		mockWriter := viper.NewMockWriter(mockCtl)
		mockWriter.EXPECT().Write(gomock.Any()).Return(tt.wr.n, tt.wr.err).AnyTimes()

		mockReader := viper.NewMockReader(mockCtl)
		firstRead := mockReader.EXPECT().Read(gomock.Any()).Return(100, nil)
		secondRead := mockReader.EXPECT().Read(gomock.Any()).Return(0, io.EOF).AnyTimes()
		gomock.InOrder(
			firstRead,
			secondRead,
		)

		mockCipher := viper.NewMockCipher(mockCtl)
		mockCipher.EXPECT().Decrypt(gomock.Any(), gomock.Any()).Return(100, nil).AnyTimes()

		errCh := make(chan *pipeErr, 2)
		pipe.dec(mockWriter, mockReader, mockCipher, errCh)
		err := <-errCh
		t.Log(err.readErr, err.writeErr)
		if !err.hasErr() || err.writeErr != tt.expected {
			t.Fatalf("TestPipeDec err,readErr:%v,writeErr:%v\n", err.readErr, err.writeErr)
		}
	}
}

func TestPipeReaderErr(t *testing.T) {
	pipe := newPipe()
	mockCtl := gomock.NewController(t)
	mockWriter := viper.NewMockWriter(mockCtl)
	mockWriter.EXPECT().Write(gomock.Any()).Return(100, nil).AnyTimes()

	mockReader := viper.NewMockReader(mockCtl)
	firstRead := mockReader.EXPECT().Read(gomock.Any()).Return(100, nil)
	secondRead := mockReader.EXPECT().Read(gomock.Any()).Return(0, io.EOF)
	gomock.InOrder(
		firstRead,
		secondRead,
	)

	mockCipher := viper.NewMockCipher(mockCtl)
	mockCipher.EXPECT().Decrypt(gomock.Any(), gomock.Any()).Return(100, nil).AnyTimes()

	errCh := make(chan *pipeErr, 2)
	pipe.dec(mockWriter, mockReader, mockCipher, errCh)
	err := <-errCh
	t.Log(err.readErr, err.writeErr)
	if !err.hasErr() || err.readErr != io.EOF {
		t.Fatalf("TestPipeReaderErr err,readErr:%v,writeErr:%v\n", err.readErr, err.writeErr)
	}
}

func BenchmarkPipe(b *testing.B) {
	pipe := newPipe()
	buf := bytes.NewBuffer(nil)
	buf.Write([]byte{119, 119, 119, 46, 98, 97, 105, 100, 117, 46, 99, 111, 109, 10})
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		//for i := 0; i < 1; i++ {
		//target := os.Stdout

		target := bytes.NewBuffer(nil)
		pr, pw := io.Pipe()
		errCh := make(chan *pipeErr, 2)
		go func() {
			encCipher := NewEncCipher("aes-gcm", "foobar", "aead")
			//encCipher := NewEncCipher("chacha20-poly1305", "foobar", "aead")
			go pipe.enc(pw, buf, encCipher, errCh)
		}()
		go func() {
			decCipher := NewDecCipher(pr, "aes-gcm", "foobar", "aead")
			//decCipher := NewDecCipher(pr, "chacha20-poly1305", "foobar", "aead")
			go pipe.dec(target, pr, decCipher, errCh)
		}()
		<-errCh
	}
}
