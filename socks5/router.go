package socks5

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"io"

	"gitlab.com/RandomCivil/common/log"
	router "gitlab.com/RandomCivil/proxy-router"
	"gitlab.com/RandomCivil/viper/internal"
	"gitlab.com/RandomCivil/viper/network"
)

type RouteConfiger interface {
	GetGfwListPath() string
	GetGeoIP2Path() string
	GetRouteStrategy() int
}

type Router struct {
	gfwList *router.GfwList
	Geoip2  *router.GeoIP2
	buf     []byte
}

func NewRouter(rc RouteConfiger) *Router {
	r := &Router{buf: make([]byte, 2)}
	if rc.GetRouteStrategy() == 1 || rc.GetRouteStrategy() == 2 {
		gfwList := router.NewGfwList(rc.GetGfwListPath())
		gfwList.ParseGfwList()
		r.gfwList = gfwList
	}
	if rc.GetRouteStrategy() == 2 {
		geoip2 := router.NewGeoIP2(rc.GetGeoIP2Path())
		r.Geoip2 = geoip2
	}
	return r
}

func (r *Router) Route(bufConn io.Reader, dnsCache *network.DnsCache, strategy int) (*Request, error) {
	parser := NewParser()
	defer parser.buf.Release()
	parser.CheckVersion(bufConn)
	parser.ParseReqMethod(bufConn)
	req, err := parser.ParseDst(bufConn)
	if err != nil {
		return nil, err
	}

	if req.typo == internal.DOMAIN_TYPE {
		buf := bytes.NewBuffer([]byte{5, 1, 0})

		domain := string(req.domain)
		if r.gfwList.Has(domain) {
			req.Direct = false

			portBytes := r.buf
			binary.BigEndian.PutUint16(portBytes, uint16(req.port))
			buf.Write([]byte{3})
			buf.Write([]byte{uint8(len(string(req.domain)))})
			buf.Write(req.domain)
			buf.Write(portBytes)

			req.Socks = buf.Bytes()
			return req, nil
		}

		err := handleDnsCache(dnsCache, req)
		if err != nil {
			return nil, err
		}

		if strategy == 1 {
			req.Direct = true
			return req, nil
		}

		r.dealGeoIP2(req)
	}
	if req.typo == internal.IPV4_TYPE || req.typo == internal.IPV6_TYPE {
		r.dealGeoIP2(req)
	}
	return req, nil
}

func (r *Router) dealGeoIP2(req *Request) {
	buf := bytes.NewBuffer([]byte{5, 1, 0})
	country := r.Geoip2.ParseGeoip2(req.ip.String())
	log.DefaultLogger.Infof("ip %s country :%s", req.ip.String(), country)

	if country == "CN" {
		req.Direct = true
	} else {
		req.Direct = false
	}

	portBytes := r.buf
	binary.BigEndian.PutUint16(portBytes, uint16(req.port))

	buf.Write([]byte{1})
	buf.Write(req.ip)
	buf.Write(portBytes)

	req.Socks = buf.Bytes()
}

func HandleRoute(bufConn *bufio.Reader, rc *RouteConn) (*io.PipeReader, *io.PipeWriter, error) {
	req, err := rc.Router.Route(bufConn, rc.DnsCache, rc.Strategy)
	if err != nil {
		return nil, nil, err
	}
	log.DefaultLogger.Infof("req:%+v, err:%v", req, err)

	if req.Direct {
		target, err := HandleRequest(req)
		if err != nil {
			return nil, nil, err
		}
		defer target.Close()

		rc.NetConn.Write([]byte{5, 0, 0, 1, 0, 0, 0, 0, 0, 0})

		errCh := make(chan error, 2)
		go network.Pipe(target, bufConn, errCh)
		go network.Pipe(rc.NetConn, target, errCh)
		<-errCh
		return nil, nil, nil
	}

	pr, pw := io.Pipe()
	go func() {
		pw.Write(req.Socks)
		network.PipeWithNoErrCh(pw, bufConn)
	}()
	return pr, pw, nil
}
