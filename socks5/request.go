package socks5

import (
	"bytes"
	"io"
	"net"
	"strconv"
	"time"

	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/internal"
)

type Request struct {
	ip     net.IP
	port   int
	typo   uint8
	domain []byte
	//for client side
	Direct bool
	Socks  []byte
}

//for server side
func NewRequest(stream io.Reader, cipher viper.Cipher) (*Request, error) {
	var rd io.Reader
	if cipher != nil {
		n, _ := cipher.Buf().ReadFrom(stream)
		encData := cipher.Buf().BytesTo(int32(n))

		payLoad := cipher.Buf().BytesTo(int32(n))
		_, err := cipher.Decrypt(payLoad, encData[:n])
		if err != nil {
			return nil, err
		}
		if !checkTs(payLoad[:internal.TS_LEN]) {
			return nil, ErrReplay
		}

		rd = bytes.NewBuffer(payLoad[internal.TS_LEN:])
	} else {
		rd = stream
	}

	parser := NewParser()
	defer parser.buf.Release()
	parser.CheckVersion(rd)
	parser.ParseReqCmd(rd)
	req, err := parser.ParseDst(rd)
	if err != nil {
		return nil, err
	}

	return req, nil
}

func HandleRequest(req *Request) (net.Conn, error) {
	addr := net.JoinHostPort(req.ip.String(), strconv.Itoa(req.port))

	var ipType string
	switch req.typo {
	case internal.IPV4_TYPE:
		ipType = "tcp"
	case internal.IPV6_TYPE:
		ipType = "tcp6"
	}

	dialer := net.Dialer{Timeout: time.Second * 10}
	target, err := dialer.Dial(ipType, addr)
	if err != nil {
		log.DefaultLogger.Errorf("dial target err:%v", err)
		return nil, err
	}

	return target, nil
}
