package socks5

import (
	"errors"
	"io"
	"net"
	"strconv"
	"time"

	"gitlab.com/RandomCivil/common/buf"
	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/viper/internal"
	"gitlab.com/RandomCivil/viper/network"
)

var (
	ErrInvalidAddrType = errors.New("invalid address type")
	ErrReplay          = errors.New("replay attack")
)

type Parser struct {
	buf *buf.Buffer
}

func NewParser() *Parser {
	return &Parser{buf: buf.New()}
}

func (p *Parser) CheckVersion(bufConn io.Reader) {
	p.buf.ReadFullFrom(bufConn, 1)
	p.buf.Advance(1)
}

func (p *Parser) ParseReqMethod(bufConn io.Reader) {
	p.buf.ReadFullFrom(bufConn, 1)
	l := int(p.buf.Byte(0))
	p.buf.Advance(1)

	p.buf.ReadFullFrom(bufConn, int32(l))
	p.buf.Advance(int32(l))
}

func (p *Parser) ParseReqCmd(bufConn io.Reader) {
	p.buf.ReadFullFrom(bufConn, 2)
	p.buf.Advance(2)
}

func (p *Parser) ParseDst(bufConn io.Reader) (*Request, error) {
	p.buf.ReadFullFrom(bufConn, 1)
	addrType := p.buf.BytesTo(1)
	p.buf.Advance(1)

	var ip, domain []byte
	var ipLen int
	var netIp net.IP
	var typo uint8

	switch uint8(addrType[0]) {
	case internal.IPV4_TYPE:
		ipLen = 4
		p.buf.ReadFullFrom(bufConn, int32(ipLen))
		ip = p.buf.BytesTo(int32(ipLen))
		p.buf.Advance(int32(ipLen))

		netIp = net.IP(ip).To4()
		typo = internal.IPV4_TYPE
	case internal.IPV6_TYPE:
		ipLen = 16
		p.buf.ReadFullFrom(bufConn, int32(ipLen))
		ip = p.buf.BytesTo(int32(ipLen))
		p.buf.Advance(int32(ipLen))

		netIp = net.IP(ip).To16()
		typo = internal.IPV6_TYPE
	case internal.DOMAIN_TYPE:
		typo = internal.DOMAIN_TYPE
		p.buf.ReadFullFrom(bufConn, 1)
		domainLen := p.buf.Byte(0)
		p.buf.Advance(1)

		p.buf.ReadFullFrom(bufConn, int32(domainLen))
		domain = p.buf.BytesTo(int32(domainLen))
		p.buf.Advance(int32(domainLen))
		log.DefaultLogger.Infof("parsed domain %s", string(domain))
	default:
		return nil, ErrInvalidAddrType
	}
	p.buf.ReadFullFrom(bufConn, 2)
	port := p.buf.BytesTo(2)

	parsedPort := (int(port[0]) << 8) | int(port[1])
	log.DefaultLogger.Debugf("ip:%v,port:%d", netIp, port)
	req := &Request{
		ip:     netIp,
		port:   parsedPort,
		typo:   typo,
		domain: domain,
	}
	return req, nil
}

func handleDnsCache(dnsCache *network.DnsCache, req *Request) error {
	domain := string(req.domain)
	if v := dnsCache.Get(domain); v != nil {
		req.ip = v.Ip
		req.typo = v.IpType
		log.DefaultLogger.Infof("dns cached domain:%s,ip:%s", string(domain), v.Ip)
	} else {
		ip, ipType, err := dnsCache.QueryDns(domain)
		req.ip = ip
		req.typo = ipType
		if err != nil {
			return network.ErrDomainNotParsed
		}
		log.DefaultLogger.Infof("domain %s ip:%s,type:%d", string(domain), ip.String(), ipType)
		dnsCache.Set(domain, &network.IpCache{Ip: req.ip, IpType: req.typo, UpdateTime: time.Now()})
	}
	return nil
}

func checkTs(tsb []byte) bool {
	ts, err := strconv.Atoi(string(tsb))
	if err != nil {
		log.DefaultLogger.Errorf("check ts err:%v", err)
		return false
	}
	now := time.Now().Unix()
	log.DefaultLogger.Infof("check ts,now:%d,ts:%d", now, ts)
	if int(now)-ts > internal.MAX_TS_GAP {
		return false
	}
	return true
}
