package socks5

import (
	"bytes"
	"testing"

	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/network"
)

var dnsCache *network.DnsCache

func init() {
	dnsCache = network.NewDnsCache(180, 180)
	dnsCache.SetDnsPriority(false)
}

func TestRoute(t *testing.T) {
	rc := &config.RouteConfig{
		GfwListPath:   viper.TestRootPath + "/gfwlist.txt",
		GeoIP2Path:    viper.TestRootPath + "/GeoLite2-Country.mmdb",
		RouteStrategy: 2,
	}
	r := NewRouter(rc)
	tests := []struct {
		b        []byte
		strategy int
		isDirect bool
	}{
		//www.gitee.com
		{[]byte{5, 1, 0, 3, 13, 119, 119, 119, 46, 103, 105, 116, 101, 101, 46, 99, 111, 109, 1, 187}, 1, true},
		//www.google.com
		{[]byte{5, 1, 0, 3, 14, 119, 119, 119, 46, 103, 111, 111, 103, 108, 101, 46, 99, 111, 109, 1, 187}, 1, false},
		//google.com
		{[]byte{5, 1, 0, 3, 10, 103, 111, 111, 103, 108, 101, 46, 99, 111, 109, 1, 187}, 1, false},

		{[]byte{5, 1, 0, 3, 13, 119, 119, 119, 46, 103, 105, 116, 101, 101, 46, 99, 111, 109, 1, 187}, 2, true},
		{[]byte{5, 1, 0, 3, 14, 119, 119, 119, 46, 103, 111, 111, 103, 108, 101, 46, 99, 111, 109, 1, 187}, 2, false},
		{[]byte{5, 1, 0, 3, 10, 103, 111, 111, 103, 108, 101, 46, 99, 111, 109, 1, 187}, 2, false},
		{[]byte{5, 1, 0, 1, 163, 177, 151, 110, 1, 187}, 2, true},
		{[]byte{5, 1, 0, 1, 13, 229, 188, 59, 1, 187}, 2, false},
	}

	for _, tt := range tests {
		rd := bytes.NewReader(tt.b)
		req, _ := r.Route(rd, dnsCache, tt.strategy)
		if req.Direct != tt.isDirect {
			t.Fatalf("TestDomainRoute conn:%v,direct expect:%v,get:%v", tt.b, tt.isDirect, req.Direct)
		}
	}
}

func TestRouteErrDst(t *testing.T) {
	rc := &config.RouteConfig{
		GfwListPath:   viper.TestRootPath + "/gfwlist.txt",
		GeoIP2Path:    viper.TestRootPath + "/GeoLite2-Country.mmdb",
		RouteStrategy: 2,
	}
	r := NewRouter(rc)
	b := []byte{5, 1, 0, 5, 13, 119, 119, 119, 46, 103, 105, 116, 101, 101, 46, 99, 111, 109, 1, 187}
	rd := bytes.NewReader(b)
	_, err := r.Route(rd, dnsCache, 2)
	if err != ErrInvalidAddrType {
		t.Fatal("TestRouteErrDst err", err)
	}
}
