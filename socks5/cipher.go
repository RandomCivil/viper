package socks5

import (
	"io"

	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/encrypt"
)

func NewEncCipher(cipherName, pwd, cipherType string) viper.Cipher {
	cipher := encrypt.NewCipher(cipherName, pwd, cipherType)
	cipher.InitEncrypt()
	log.DefaultLogger.Debugf("enc iv %v", cipher.IV())
	return cipher
}

func NewDecCipher(stream io.Reader, cipherName, pwd, cipherType string) viper.Cipher {
	cipher := encrypt.NewCipher(cipherName, pwd, cipherType)
	ivLen := cipher.IVLen()

	cipher.Buf().ReadFullFrom(stream, int32(ivLen))
	iv := cipher.Buf().BytesTo(int32(ivLen))
	cipher.Buf().Advance(int32(ivLen))

	cipher.InitDecrypt(iv)
	return cipher
}
