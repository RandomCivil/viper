package socks5

import (
	"bufio"
	"io"
	"sync/atomic"

	"github.com/xtaci/smux"
	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/viper/internal"
	"gitlab.com/RandomCivil/viper/network"
)

var (
	sid = &sessionId{i: 0}
)

type sessionId struct {
	i int32
}

func (m *sessionId) index() int32 {
	return atomic.AddInt32(&m.i, 1)
}

type RouteConn struct {
	NetConn   io.ReadWriteCloser
	Strategy  int
	Router    *Router
	DnsCache  *network.DnsCache
	ReturnCh  chan struct{}
	PipeCh    chan *PipeConn
	BufConnCh chan io.Reader
}

type PipeConn struct {
	Pr *io.PipeReader
	Pw *io.PipeWriter
}

func HandleClientConn(rc *RouteConn) {
	bufConn := bufio.NewReader(rc.NetConn)

	rc.NetConn.Write([]byte{internal.SOCKS5_VER, byte(0)})

	parser := NewParser()
	parser.CheckVersion(bufConn)
	parser.ParseReqMethod(bufConn)
	parser.buf.Release()

	var (
		pr  *io.PipeReader
		pw  *io.PipeWriter
		err error
	)
	if rc.Strategy > 0 {
		pr, pw, err = HandleRoute(bufConn, rc)
		if err != nil {
			log.DefaultLogger.Errorf("HandleClientConn err:%v", err)
			rc.ReturnCh <- struct{}{}
			return
		}
		//direct request
		if pr == nil {
			rc.ReturnCh <- struct{}{}
			return
		}
		rc.PipeCh <- &PipeConn{Pr: pr, Pw: pw}
		return
	}
	rc.BufConnCh <- bufConn
}

func HandleServerConn(session *Session, streamer Streamer) {
	sessionId := sid.index()
	errCh := make(chan error, 1)
	for {
		if stream, err := session.Se.AcceptStream(); err == nil {
			go func() {
				socks5Stream := &Stream{
					stream:     stream,
					cipher:     session.CipherName,
					pwd:        session.Pwd,
					cipherType: session.CipherType,
					dnsCache:   session.DnsCache,
				}
				if v, ok := stream.(*smux.Stream); ok {
					log.DefaultLogger.Infof("accept stream %d in session %d which stream num is %d", v.ID(), sessionId, session.Se.NumStreams())
				}
				errCh <- streamer.Handle(socks5Stream)

				select {
				case e := <-errCh:
					if e != nil {
						log.DefaultLogger.Errorf("handle stream err:%v", e)
						ip := network.RemoteIP(session.Se)
						log.DefaultLogger.Errorf("invalid ip:%s", ip)
						BlockIps.Store(ip, true)

						session.Se.Close()
						log.DefaultLogger.Infof("session %d is closed,err:%v", sessionId, e)
						break
					}
				}
			}()
		} else {
			session.Se.Close()
			log.DefaultLogger.Infof("session %d is closed,err:%v", sessionId, err)
			break
		}
	}
}
