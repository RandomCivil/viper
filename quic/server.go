package quic

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"strconv"
	"time"

	quicGo "github.com/quic-go/quic-go"
	"gitlab.com/RandomCivil/common/log"
	sn "gitlab.com/RandomCivil/common/signal"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/network"
	"gitlab.com/RandomCivil/viper/socks5"
	"golang.org/x/net/http2"
)

type quicServer struct {
	signal *sn.Signal
	config *config.QuicServerConfig
	done   chan struct{}
	ln     *quicGo.Listener
	dc     *network.DnsCache
}

func NewQuicServer(config *config.QuicServerConfig) *quicServer {
	dc := network.NewDnsCache(config.DnsCleanInterval, config.DnsCleanInterval)
	dc.SetDnsPriority(config.PreferIpv6 == 1)

	s := &quicServer{
		signal: sn.NewSignal(),
		config: config,
		done:   make(chan struct{}, 1),
		dc:     dc,
	}
	return s
}

type AcceptedConn struct {
	Sess quicGo.Connection
	Err  error
}

func (q *quicServer) handleConn(session quicGo.Connection) {
	wrapSess := &socks5.Session{
		Se:       &network.QuicSession{Se: network.QuicConcreteSession{Connection: session}},
		DnsCache: q.dc,
	}
	socks5.HandleServerConn(wrapSess, &socks5.PlainStream{})
}

func (q *quicServer) Serve(certPEMBlock, keyPEMBlock, authCertBytes []byte) error {
	cert, err := tls.X509KeyPair(certPEMBlock, keyPEMBlock)
	if err != nil {
		return err
	}

	clientCertPool := x509.NewCertPool()
	ok := clientCertPool.AppendCertsFromPEM(authCertBytes)
	if !ok {
		return errors.New("failed to parse root certificate")
	}

	tlsCfg := &tls.Config{
		Certificates: []tls.Certificate{cert},
		NextProtos:   []string{http2.NextProtoTLS, "quic-viper"},
		ClientAuth:   tls.RequireAndVerifyClientCert,
		ClientCAs:    clientCertPool,
	}

	quicConfig := &quicGo.Config{
		MaxIdleTimeout:       60 * time.Second,
		HandshakeIdleTimeout: 30 * time.Second,
	}
	listener, err := quicGo.ListenAddr(":"+strconv.Itoa(q.config.Listen), tlsCfg, quicConfig)
	if err != nil {
		panic(err)
	}

	q.ln = listener
	q.done <- struct{}{}

	go q.signal.Handle(listener)
	for {
		sess, err := listener.Accept(context.Background())
		if err != nil {
			log.DefaultLogger.Errorf("accept err:%v", err)
			return err
		}
		go q.handleConn(sess)
	}
}
