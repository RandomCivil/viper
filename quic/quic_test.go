package quic

import (
	"io"
	"net"
	"net/http"
	"os"
	"testing"
	"time"

	"gitlab.com/RandomCivil/common/cert"
	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/socks5"
)

var (
	KEY, CERT         []byte
	AuthCert, AuthKey []byte

	WRONG_CERT = `-----BEGIN CERTIFICATE-----
MIIDlzCCAn+gAwIBAgIUEI9gylPDUOyst4PtAmhcUPO3/JIwDQYJKoZIhvcNAQEL
BQAwWzELMAkGA1UEBhMCY24xCjAIBgNVBAgMAWExCjAIBgNVBAcMAWIxCjAIBgNV
BAoMAWMxCjAIBgNVBAsMAWQxCjAIBgNVBAMMAWUxEDAOBgkqhkiG9w0BCQEWAWYw
HhcNMTkxMDE0MTMzMzMzWhcNMjAxMDEzMTMzMzMzWjBbMQswCQYDVQQGEwJjbjEK
MAgGA1UECAwBYTEKMAgGA1UEBwwBYjEKMAgGA1UECgwBYzEKMAgGA1UECwwBZDEK
MAgGA1UEAwwBZTEQMA4GCSqGSIb3DQEJARYBZjCCASIwDQYJKoZIhvcNAQEBBQAD
ggEPADCCAQoCggEBAOo6SC5GAdqu79bel90d3JQgrMIK4fXWZmeEkRXp5Zg6/T3O
O0NF2nO7Zr6BILA8Mu1G9TWIY1sVYFXXbd4f+eJV/9m0+BWG+xp2KKcBMQKFY6hj
Y6T0avvZZvoqyvHHqOZFNVkJ8aa0YklPtuy7LYhhJoydsRgSlfJsu70LN4DLyFLD
Yuf5xRfiBL+YjHjkP8r6DEexgXIlmYDScstJVzML042wqSTk1a7egpw9E8AFaU4W
RZoWNP97Ao5/Qk6bx62BNzEskUxlwh87zfTVyr5wChaQVwg5GnJ5Peoo8aAETd+u
M38X/JdSNzzB+7S4WCtom4eCKk8Uj7d50742tWcCAwEAAaNTMFEwHQYDVR0OBBYE
FAoIn5uQ24rhqwBvA3lGl0cMKTHdMB8GA1UdIwQYMBaAFAoIn5uQ24rhqwBvA3lG
l0cMKTHdMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBALyUMCSv
W8ANcxdvpCTaJYpJPXWtU9/2GsEa+fdRuCImVKHf6dt4qDbrkPvLaOpivsqDNpmo
Zio+dwc/1uhz+OD1/Bbn7a7r/DmYEUR9C0Bv/pvUTaksWUpzCXacPNfhT2kku4CC
gRDUprTBom96fhGk5JIshCWvsCTCoLAhOWTgqbnflSttxnG+AgxTSw3UWTLwby4I
FtzVypPi8phbVe6Mc+7Rm4tFt85jnerv4/I/CUuzWa4Q/vFfpyWCAFlBeLW7GFaj
QTpyzMltpsd43JLUlxAaQl0NKQjNKaWDH+jwMEiqvyijxllqOY3QmqqXovukaIv5
qj5iAyzbqnFct4k=
-----END CERTIFICATE-----
`
	WRONG_KEY = `-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDKIMOAsTJuuyr+
o/uygbt+T14kYBSSUnfdHwWM5CqhzhkXpUWXZqI2evO38eW1VKa2o9/pgEhuE/FL
t6tTZIjjM0pc9I8MJOMmDT/JhB5iRTJLWkhzLdk15YgxSbqRWuveQ7kO9NIiLLOm
A76grUNcU3it7OZ+NO6yxG00YflcFkTyH6Ly38OBXeYoL/AYs0to7bFfyw7HrQKl
0E5Knjmh6/JYGLJ4/RYuFNzBegCbcOFrGodrP2amLAxZVoY82LsaWoCd2s/hD0T1
2x9J2nQsE/wzIYBjlZDJjTLhb56jV9jOLb1jmoXOETO/k9pkDZWhVs044sLLoXT5
rqeavdJZAgMBAAECggEAYOesUzqg7y2Uw1hjB4XjGf97Jv1ZX31iMgT5aD7LoZ9p
sEhwQNHX2z6/MeUrN/BJrS6L3yq9jujIuoBoBycf6a1hbYdiPXRB4rBfcIGC/6Il
JrM2IOJOTOSqktbCN4Xbp0fYpS8Cv5em2WTaM7ZdoRnztY7qQn+mINbDdGW0Y0z/
ywY0YE8QV7EfsYs/zloNMkuetiFDWNL6BoC1eqiGBe8SvKpQalV8oE8by+VGIjTA
9dh1ZWarF4+0MXAEZaJTnbJeGuxpha6lPiwcYbx0nb7kwGNBHq+pLNT+Kb6ZCaIz
r33lU11NA40pkTKyt8JZ/x2m3Xomh50F9cEgN8LncQKBgQDjyS5OmxtvyodGE8wX
dORBfZV4y/ePsaVpdZaok+AKfIiZYylielhAI+p8NvQO37THsYLbZ6OI77fro0le
qWwKjlCLknE3YvGKeb0WscmCofLOJK9G59xgF2JYlEQN9aa/ov930vjH5aAdzeam
vztrRmhekZkzSNwczSjf8gmrMwKBgQDjKgB9MOxgLItH1boJpKxuXG1+mcIECgRy
S61Hf8+uCMO2di5Vh6PE4eK1lBBGeTGQboZGUOGCJbINV8wfBg51sCIbyM8lCg8r
TqRqtabXcvYsI7H+BaQSlADZ/F8iMbC6pE65tT4LNMi2iCVMlppKaZmddfgmIXVr
UIryHhHsQwKBgGuVNDrKBT5acGOHrCgT/E52Kud/pjAG3jId6Ic8OF+bi8SuUfiC
AjZhADmklsaDn9WFIGHR5Coj1kEDwP5llHKz3pTdGH3pTF1zu/wBAkShipUUPHiH
v8zenoNpm27ga94s9O26BegbqvEIs3xj64NyII8xwyK2scckmr6SiUCDAoGAa4rM
5Heuz/EGlyF1i28sOqsDIzZDhYoAhOOSyxCVDz0S+mSElvU58NFHdNL9yX9Cma/Z
XZyYxfZ2jp6MAfvqCIkz/JdaiZxzhfsbF3Gb5M+F/2t+rlWZUTpEFO7HUvbXReTX
aE+HaeK5SsC1d8askKHhmYvpyJN6dS2SOgFGVuUCgYEAxJZE9cZgwiFtZwchXVcU
x6pa6qbSxhbRNV9SsGnrGK1fw1zkPwbQ8wjwO4XiQYKIKEmjkFwKnNYH2Pr6YOG5
35eILyPg7l1Sw0CRjST5N3PeFI6EqXC2oFYbgSufXI7QoFv0MyOH6eHgTBuUnc4r
An9a3kGNEZFuhiFJD7euL9Q=
-----END PRIVATE KEY-----
`
)

func init() {
	CERT, KEY = cert.GenCert()
	AuthCert, AuthKey = cert.GenCert()
}

func TestMain(m *testing.M) {
	ms := &viper.MockServer{
		StopCh: make(chan struct{}, 1),
		DoneCh: make(chan struct{}, 1),
	}
	go ms.Start(8845, 8082)
	<-ms.DoneCh

	r := m.Run()
	os.Exit(r)
	ms.Stop()
}

func newServerDefaultConf() *config.QuicServerConfig {
	c := new(config.QuicServerConfig)
	c.Listen = 0
	c.PreferIpv6 = 0
	c.DnsCleanInterval = 60
	return c
}

func newClientDefaultConf() *config.QuicClientConfig {
	c := new(config.QuicClientConfig)
	c.Listen = 0
	c.RouteStrategy = 0
	c.GeoIP2Path = viper.TestRootPath + "/GeoLite2-Country.mmdb"
	c.GfwListPath = viper.TestRootPath + "/gfwlist.txt"
	c.ConnNum = 3
	c.ConnStrategy = "lsf"
	c.DnsCleanInterval = 60
	return c
}

func TestAuth(t *testing.T) {
	cc := newClientDefaultConf()
	server, client := makeBothStart(t, newServerDefaultConf(), cc)
	defer server.ln.Close()

	tests := []struct {
		reqUrl string
	}{
		{"https://localhost:8845"},
		{"https://127.0.0.1:8845"},
		{"http://localhost:8082"},
		{"http://127.0.0.1:8082"},
	}

	var (
		resp *http.Response
		err  error
	)
	for _, tt := range tests {
		resp, err = viper.RequestViaTransport(t, client.ln.Addr().String(), tt.reqUrl)
		if err != nil {
			t.Fatal("TestSuccessAuth", err)
		}
		t.Log(resp.StatusCode, err)
		t.Log(resp.Proto)
		resp.Body.Close()
	}
	client.ln.Close()

	AuthCert, AuthKey = cert.GenCert()
	cc.ServerAddr = server.ln.Addr().String()
	client = clientStart(cc, t)

	resp, err = viper.RequestViaTransport(t, client.ln.Addr().String(), "https://localhost:8845")
	t.Log(err)
	if err == nil {
		t.Fatal("TestFailAuth", err)
	}
	client.ln.Close()
}

func TestConnFlex(t *testing.T) {
	cc := newClientDefaultConf()
	cc.ConnStrategy = "flex"
	cc.MaxStreamPerSession = 2
	server, client := makeBothStart(t, newServerDefaultConf(), cc)
	defer server.ln.Close()
	defer client.ln.Close()

	var (
		resp *http.Response
		err  error
		url  = "https://localhost:8845"
	)
	for i := 0; i < 5; i++ {
		resp, err = viper.RequestViaTransport(t, client.ln.Addr().String(), url)
		if err != nil {
			t.Fatal("TestConnFlex fail", err)
		}
		t.Log(resp.StatusCode, err)
		t.Log(resp.Proto)
		resp.Body.Close()
	}
}

func TestWrongTarget(t *testing.T) {
	c := newClientDefaultConf()
	c.ServerAddr = "127.0.0.1:8000"
	c.ConnNum = 1
	client, err := NewQuicClient(c, AuthCert, AuthKey)
	if err == nil {
		t.Fatal("TestWrongTarget err")
	}
	t.Log(client, err)
}

func TestRouteStrategy(t *testing.T) {
	tests := []struct {
		reqUrl   string
		strategy int
	}{
		{"https://www.baidu.com", 1},
		{"https://www.amazon.com", 1},
		{"https://www.baidu.com", 2},
		{"https://www.amazon.com", 2},
	}
	sc := newServerDefaultConf()
	cc := newClientDefaultConf()

	for _, tt := range tests {
		cc.RouteStrategy = tt.strategy
		server, client := makeBothStart(t, sc, cc)
		resp, err := viper.RequestViaTransport(t, client.ln.Addr().String(), tt.reqUrl)
		if err != nil {
			t.Fatal("TestRouteStrategy fail", err)
		}
		t.Log(resp.StatusCode, err)
		t.Log(resp.Proto)
		resp.Body.Close()
		client.ln.Close()
		server.ln.Close()
		time.Sleep(100 * time.Millisecond)
	}
}

func TestInvalidAddrType(t *testing.T) {
	server := startServer(newServerDefaultConf(), t)
	defer server.ln.Close()
	config := newClientDefaultConf()

	strategys := []int{0, 2}
	for _, s := range strategys {
		config.ServerAddr = server.ln.Addr().String()
		config.RouteStrategy = s
		client := clientStart(config, t)

		target, err := net.Dial("tcp", client.ln.Addr().String())
		if err != nil {
			t.Fatal(err)
		}

		target.Write([]byte{5, 2, 0, 2})

		recv := make([]byte, 2)
		io.ReadAtLeast(target, recv, 2)
		t.Log("auth method", recv)

		target.Write([]byte{5, 1, 0, 5, 14, 215, 177, 39, 80, 0})

		b := make([]byte, 100)
		n, err := io.ReadFull(target, b)
		t.Log(n, err, b[:n])
		if err == nil {
			t.Fatal("TestInvalidAddrType fail")
		}
		client.ln.Close()

		socks5.BlockIps.Delete("127.0.0.1")
		socks5.BlockIps.Delete("::1")
	}
}

func TestClientWrongCert(t *testing.T) {
	c := newClientDefaultConf()
	_, err := NewQuicClient(c, []byte(WRONG_CERT), []byte(WRONG_KEY))
	t.Log(err)
	if err == nil {
		t.Fatal("TestClientWrongCert error")
	}
}

func TestServerAuthCert(t *testing.T) {
	c := newServerDefaultConf()
	server := NewQuicServer(c)
	err := server.Serve([]byte(CERT), []byte(KEY), []byte("lovelittlefatcat"))
	t.Log(err)
	if err == nil {
		t.Fatal("TestServerWrongCert error")
	}
}

func TestServerWrongCert(t *testing.T) {
	c := newServerDefaultConf()
	server := NewQuicServer(c)
	err := server.Serve([]byte(WRONG_CERT), []byte(WRONG_KEY), []byte("lovelittlefatcat"))
	t.Log(err)
	if err == nil {
		t.Fatal("TestServerWrongCert error")
	}
}

func startServer(serverConfig *config.QuicServerConfig, t *testing.T) *quicServer {
	serverConfig.DnsCleanInterval = 60
	server := NewQuicServer(serverConfig)
	go func() {
		server.Serve(CERT, KEY, AuthCert)
	}()
	<-server.done
	t.Log("server started")
	return server
}

func clientStart(clientConfig *config.QuicClientConfig, t *testing.T) *quicClient {
	client, _ := NewQuicClient(clientConfig, AuthCert, AuthKey)
	go func() {
		client.Serve()
	}()
	<-client.done
	t.Log("client started")
	return client
}

func makeBothStart(t *testing.T, sc *config.QuicServerConfig, cc *config.QuicClientConfig) (*quicServer, *quicClient) {
	done := make(chan struct{})
	ch := make(chan struct{})
	server := NewQuicServer(sc)
	var client *quicClient

	go func() {
		server.Serve(CERT, KEY, AuthCert)
	}()
	go func() {
		<-server.done
		t.Log("server start", server.ln.Addr().String())
		cc.ServerAddr = server.ln.Addr().String()
		client, _ = NewQuicClient(cc, AuthCert, AuthKey)
		ch <- struct{}{}
		client.Serve()
	}()
	go func() {
		<-ch
		<-client.done
		done <- struct{}{}
	}()
	<-done
	t.Log("client start")
	return server, client
}
