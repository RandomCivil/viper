package quic

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"io"
	"net"
	"runtime"
	"strconv"
	"time"

	quicGo "github.com/quic-go/quic-go"
	"gitlab.com/RandomCivil/common/log"
	sn "gitlab.com/RandomCivil/common/signal"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/network"
	"gitlab.com/RandomCivil/viper/socks5"
)

type quicClient struct {
	config   *config.QuicClientConfig
	tlsCfg   *tls.Config
	signal   *sn.Signal
	done     chan struct{}
	ln       net.Listener
	dc       *network.DnsCache
	router   *socks5.Router
	dialFunc func(ctx context.Context, i int32) *network.DialTarget
	sess     *network.Session
}

func NewQuicClient(conf *config.QuicClientConfig, certPEMBlock, keyPEMBlock []byte) (*quicClient, error) {
	cert, err := tls.X509KeyPair(certPEMBlock, keyPEMBlock)
	if err != nil {
		return nil, err
	}
	clientCertPool := x509.NewCertPool()
	ok := clientCertPool.AppendCertsFromPEM(certPEMBlock)
	if !ok {
		return nil, errors.New("failed to parse root certificate")
	}

	tlsCfg := &tls.Config{
		Certificates:       []tls.Certificate{cert},
		ClientCAs:          clientCertPool,
		InsecureSkipVerify: true,
		NextProtos:         []string{"quic-viper"},
	}
	quicConf := &quicGo.Config{
		HandshakeIdleTimeout: time.Duration(5) * time.Second,
	}
	dialFunc := func(ctx context.Context, i int32) *network.DialTarget {
		r := new(network.DialTarget)
		quicSess, err := quicGo.DialAddr(ctx, conf.ServerAddr, tlsCfg, quicConf)
		if err == nil {
			r.Target = network.QuicConcreteSession{Connection: quicSess}
			r.Done = true
		}
		return r
	}
	sessConfig := &network.SessionConfig{
		Strategy:            conf.ConnStrategy,
		Num:                 conf.ConnNum,
		MaxStreamPerSession: conf.MaxStreamPerSession,
	}
	session, err := network.NewSession(sessConfig, dialFunc)
	if err != nil {
		return nil, err
	}

	r := socks5.NewRouter(conf)

	dc := network.NewDnsCache(conf.DnsCleanInterval, conf.DnsCleanInterval)
	dc.SetDnsPriority(false)

	c := &quicClient{
		config:   conf,
		signal:   sn.NewSignal(),
		done:     make(chan struct{}, 1),
		dc:       dc,
		router:   r,
		dialFunc: dialFunc,
		sess:     session,
	}
	return c, nil
}

func (c *quicClient) clientConn(netConn net.Conn) {
	netConn.SetDeadline(time.Now().Add(60 * time.Second))
	defer netConn.Close()

	rc := &socks5.RouteConn{
		NetConn:   netConn,
		Strategy:  c.config.RouteStrategy,
		Router:    c.router,
		DnsCache:  c.dc,
		ReturnCh:  make(chan struct{}, 1),
		PipeCh:    make(chan *socks5.PipeConn, 1),
		BufConnCh: make(chan io.Reader, 1),
	}
	var concreteConn io.Reader
	socks5.HandleClientConn(rc)
	select {
	case <-rc.ReturnCh:
		return
	case pipeConn := <-rc.PipeCh:
		concreteConn = pipeConn.Pr
		defer pipeConn.Pw.Close()
		defer pipeConn.Pr.Close()
	case bufConn := <-rc.BufConnCh:
		concreteConn = bufConn
	}

	session := c.sess.Fetch()
	if session == nil {
		return
	}
	stream, err := session.OpenStream()
	if err != nil {
		log.DefaultLogger.Errorf("make stream err:%v", err)
		session, stream = c.sess.MakeStream(session, c.config.ConnStrategy)
	}
	c.sess.Ch <- session
	defer session.CloseStream(stream)

	errCh := make(chan error, 2)
	go network.Pipe(stream, concreteConn, errCh)
	go network.Pipe(netConn, stream, errCh)
	err = <-errCh
	log.DefaultLogger.Infof("stream in session %d pipe err %v", session.GetIndex(), err)
}

func (c *quicClient) Serve() error {
	runtime.GC()
	l, err := net.Listen("tcp", ":"+strconv.Itoa(c.config.Listen))
	if err != nil {
		log.DefaultLogger.Errorf("listen err:%v", err)
		return errors.New("quic listen error")
	}

	c.ln = l
	c.done <- struct{}{}
	log.DefaultLogger.Infof("client start at %s", l.Addr().String())

	go c.signal.Handle(l)
	for {
		conn, err := l.Accept()
		if err != nil {
			log.DefaultLogger.Errorf("accept err:%v", err)
			return err
		}

		go c.clientConn(conn)
	}
}
