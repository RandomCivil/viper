FROM theviper/proxy-resource:latest as geolite
FROM golang:1.19-alpine

WORKDIR /go/src/gitlab.com/RandomCivil/viper

RUN apk add --no-cache gcc musl-dev bash curl openssl make git

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .

COPY --from=geolite /root/gfwlist.txt /go/src/viper/gfwlist.txt
COPY --from=geolite /root/GeoLite2-Country.mmdb /go/src/viper/GeoLite2-Country.mmdb

# RUN rm -r ~/.cache/go-build/
