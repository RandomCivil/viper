NAME = viper
BIN_DIR = $(shell pwd)
BUILD_TIME = $(shell date -u +%Y-%m-%dT%H:%M:%S%z)
GO_VER = $(shell go version)
GIT_COMMIT = $(shell git rev-parse --short=8 HEAD || echo unknown)
PACKAGE_NAMESPACE = gitlab.com/RandomCivil/common

GOBUILD=CGO_ENABLED=0 go build -ldflags '-X "$(PACKAGE_NAMESPACE).GoVersion=$(GO_VER)" \
		-X "$(PACKAGE_NAMESPACE).BuildTime=$(BUILD_TIME)" \
		-X "$(PACKAGE_NAMESPACE).GitCommit=$(GIT_COMMIT)" \
		-w -s'

build:
	$(GOBUILD) -o $(BIN_DIR)/$(NAME) ./cmd
