package internal

const (
	SESSION_CLEAN_INTERVAL = 60 * 1000
	DNS_CACHE_EXPIRE       = 24 * 60 * 60

	SOCKS5_VER     = uint8(5)
	MAX_DOMAIN_LEN = int(4 + 1 + 253 + 2)
	MAX_IPV6_LEN   = int(4 + 1 + 8*4 + 7 + 2)

	CONNECT_CMD = uint8(1)
	BIND_CMD    = uint8(2)

	IPV4_TYPE   = uint8(1)
	IPV6_TYPE   = uint8(4)
	DOMAIN_TYPE = uint8(3)

	TS_LEN       = 10
	MAX_TS_GAP   = 60
	MAX_OVERHEAD = 16
)
