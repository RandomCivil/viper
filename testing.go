package viper

import (
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"testing"
	"time"

	"gitlab.com/RandomCivil/common/cert"
	"gitlab.com/RandomCivil/common/log"
	"golang.org/x/net/http2"
)

var TestRootPath string

func init() {
	log.Level = 3
	log.New()

	ciEnv := os.Getenv("CI")
	if ciEnv == "true" {
		TestRootPath = "/go/src/viper"
	} else {
		TestRootPath = os.Getenv("HOME")
	}
}

func WriteTimeout(conn net.Conn, t *testing.T) {
	conn.SetWriteDeadline(time.Now().Add(3 * time.Second))
	for {
		n, err := conn.Write([]byte("WRITE TIMEOUT TEST"))
		if err == nil {
			time.Sleep(time.Second * 1)
			continue
		}
		if n != 0 {
			t.Fatalf("wrote %d; want 0", n)
		}
		t.Log(n, err)
		break
	}
}

func ReadTimeout(conn net.Conn, t *testing.T) {
	conn.SetReadDeadline(time.Now().Add(3 * time.Second))
	var err error

	d := time.Duration(10 * time.Second)
	if err = conn.SetReadDeadline(time.Now().Add(d)); err != nil {
		return
	}
	b := make([]byte, 256)
	var n int
	n, err = conn.Read(b)
	if n != 0 || err == nil || !err.(net.Error).Timeout() {
		t.Fatalf("Read did not return (0, timeout): (%d, %v)", n, err)
		return
	}
}

func ReadEOF(conn net.Conn, t *testing.T) {
	one := make([]byte, 1)
	conn.SetReadDeadline(time.Now().Add(3 * time.Second))
	if _, err := conn.Read(one); err == io.EOF {
		t.Log(err)
	} else {
		t.Fatal("conn read no EOF")
	}
}

type MockServer struct {
	StopCh, DoneCh chan struct{}
	ln, h2ln       net.Listener
}

func (m *MockServer) Stop() {
	m.StopCh <- struct{}{}
}

func (m *MockServer) handleConn(conn net.Conn) {
	defer conn.Close()
	if tlsConn, ok := conn.(*tls.Conn); ok {
		e := tlsConn.Handshake()
		log.DefaultLogger.Infof("handshake err:%v", e)
		state := tlsConn.ConnectionState()
		log.DefaultLogger.Infof("state", state.HandshakeComplete, state.CipherSuite)
	}

	//conn.Write([]byte("HTTP/1.1 200 hello\r\n\r\n"))
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("hello"))
	})
	h2Server := http2.Server{}
	h2Server.ServeConn(conn, &http2.ServeConnOpts{
		BaseConfig: &http.Server{},
		Handler:    handler,
	})
}

func (m *MockServer) tlsListenLoop() {
	for {
		conn, err := m.h2ln.Accept()
		if err != nil {
			log.DefaultLogger.Errorf("tls listen err:%v", err)
			return
		}
		go m.handleConn(conn)
		select {
		case <-m.StopCh:
			return
		default:
		}
	}
}

func (m *MockServer) Start(httpsPort, httpPort int) {
	mockCert, mockKey := cert.GenCert()
	cert, err := tls.X509KeyPair(mockCert, mockKey)
	if err != nil {
		return
	}
	tlsConfig := &tls.Config{
		Certificates:             []tls.Certificate{cert},
		NextProtos:               []string{http2.NextProtoTLS},
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.X25519},
		PreferServerCipherSuites: true,
	}

	h2ln, err := tls.Listen("tcp", ":"+strconv.Itoa(httpsPort), tlsConfig)
	if err != nil {
		panic(err)
	}
	m.h2ln = h2ln

	ln, err := net.Listen("tcp", ":"+strconv.Itoa(httpPort))
	if err != nil {
		panic(err)
	}
	m.ln = ln

	go func() {
		log.DefaultLogger.Infof("mock http2 server start")
		m.tlsListenLoop()
	}()

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte("hello http2"))
	})

	go func() {
		log.DefaultLogger.Infof("mock http server start")
		err := http.Serve(ln, handler)
		if err != nil {
			panic(err)
		}
	}()

	m.DoneCh <- struct{}{}
	select {
	case <-m.StopCh:
		return
	}
}

func RequestViaTransport(t *testing.T, proxyAddr string, reqUrl string) (*http.Response, error) {
	proxyUrl, _ := url.Parse("socks5://" + proxyAddr)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		Proxy:           http.ProxyURL(proxyUrl),
		IdleConnTimeout: 100 * time.Millisecond,
	}

	if err := http2.ConfigureTransport(tr); err != nil {
		fmt.Fprintf(os.Stderr, "error configuring transport: %v\n", err)
		os.Exit(1)
	}

	client := &http.Client{Transport: tr}
	resp, err := client.Get(reqUrl)
	return resp, err
}
