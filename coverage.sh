#! /bin/sh
# ./coverage.sh $branch $upload_url

BRANCH=$1
URL=$2
echo 'branch = ' $BRANCH

if [ "$BRANCH" = "master" ]; then 
    go tool cover --html=coverage.txt -o=coverage.html
    curl -kv $URL -F file=@./coverage.html
fi;
