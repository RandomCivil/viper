package kcp

import (
	"io"
	"net"
	"os"
	"testing"
	"time"

	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/socks5"
)

func TestMain(m *testing.M) {
	ms := &viper.MockServer{
		StopCh: make(chan struct{}, 1),
		DoneCh: make(chan struct{}, 1),
	}
	go ms.Start(8444, 8081)
	<-ms.DoneCh

	r := m.Run()
	os.Exit(r)
	ms.Stop()
}

func newServerDefaultConf() *config.KcpServerConfig {
	c := new(config.KcpServerConfig)
	c.Pwd = "foobar"
	c.Listen = 0
	c.KeepAliveInterval = 10
	c.SndWnd = 1024
	c.RcvWnd = 1024
	c.PreferIpv6 = 0
	c.Mode = "fast"
	c.DnsCleanInterval = 60
	return c
}

func newClientDefaultConf() *config.KcpClientConfig {
	c := new(config.KcpClientConfig)
	c.Pwd = "foobar"
	c.Listen = 0
	c.ConnNum = 2
	c.ConnStrategy = "lsf"
	c.KeepAliveInterval = 10
	c.SndWnd = 1024
	c.RcvWnd = 1024
	c.Mode = "fast"
	c.SmuxKeepAliveTimeout = 10000
	c.SmuxKeepAliveInterval = 2000
	c.RouteStrategy = 0
	c.DnsCleanInterval = 60
	c.GeoIP2Path = viper.TestRootPath + "/GeoLite2-Country.mmdb"
	c.GfwListPath = viper.TestRootPath + "/gfwlist.txt"
	return c
}

func TestSuccessAuth(t *testing.T) {
	server, client := makeBothStart(t, newServerDefaultConf(), newClientDefaultConf())
	defer client.ln.Close()
	defer server.ln.Close()

	tests := []struct {
		reqUrl string
	}{
		{"https://localhost:8444"},
		{"https://127.0.0.1:8444"},
		{"http://localhost:8081"},
		{"http://127.0.0.1:8081"},
	}

	for _, tt := range tests {
		resp, err := viper.RequestViaTransport(t, client.ln.Addr().String(), tt.reqUrl)
		if err != nil {
			t.Fatal("TestSuccessAuth", err)
		}
		t.Log(resp.StatusCode, err)
		t.Log(resp.Proto)
		resp.Body.Close()
	}
}

func TestRouteStrategy(t *testing.T) {
	tests := []struct {
		reqUrl   string
		strategy int
	}{
		{"https://www.baidu.com", 1},
		{"https://www.amazon.com", 1},
		{"https://www.baidu.com", 2},
		{"https://www.amazon.com", 2},
	}
	sc := newServerDefaultConf()
	cc := newClientDefaultConf()

	for _, tt := range tests {
		cc.RouteStrategy = tt.strategy
		server, client := makeBothStart(t, sc, cc)
		resp, err := viper.RequestViaTransport(t, client.ln.Addr().String(), tt.reqUrl)
		if err != nil {
			t.Fatal("TestRouteStrategy fail", err)
		}
		t.Log(resp.StatusCode, err)
		t.Log(resp.Proto)
		resp.Body.Close()
		client.ln.Close()
		server.ln.Close()
		time.Sleep(100 * time.Millisecond)
	}
}

func TestWrongAddr(t *testing.T) {
	server, client := makeBothStart(t, newServerDefaultConf(), newClientDefaultConf())
	defer server.ln.Close()
	defer client.ln.Close()

	resp, err := viper.RequestViaTransport(t, client.ln.Addr().String(), "https://127.0.0.2:12345")
	t.Log(err)
	if err == nil {
		t.Fatalf("TestWrongAddr error,err:%v,resp:%v", err, resp)
	}
}

func TestWrongDomain(t *testing.T) {
	server, client := makeBothStart(t, newServerDefaultConf(), newClientDefaultConf())
	defer server.ln.Close()
	defer client.ln.Close()

	resp, err := viper.RequestViaTransport(t, client.ln.Addr().String(), "https://fjwofjowfweo.com")
	t.Log(err)
	if err == nil {
		t.Fatalf("TestWrongDomain error,err:%v,resp:%v", err, resp)
	}
}

func TestFailAuth(t *testing.T) {
	server := startServer(newServerDefaultConf(), t)
	defer server.ln.Close()

	tests := []struct {
		reqUrl, pwd string
	}{
		{"https://localhost:8444", "123"},
		{"http://localhost:8081", "123"},
	}

	for _, tt := range tests {
		config := newClientDefaultConf()
		config.Pwd = tt.pwd
		config.ServerAddr = server.ln.Addr().String()
		client := startClient(config, t)

		conn, err := net.Dial("tcp", client.ln.Addr().String())
		if err != nil {
			t.Fatalf("err:%v", err)
		}

		conn.Write([]byte{5, 2, 0, 2})

		recv := make([]byte, 2)
		io.ReadAtLeast(conn, recv, 2)
		viper.WriteTimeout(conn, t)
		conn.Close()
		client.ln.Close()
	}
}

func TestMakeStream(t *testing.T) {
	cc := newClientDefaultConf()
	cc.ConnNum = 1
	cc.SmuxKeepAliveTimeout = 1000
	cc.SmuxKeepAliveInterval = 200
	server, client := makeBothStart(t, newServerDefaultConf(), cc)
	defer client.ln.Close()
	defer server.ln.Close()

	tests := []struct {
		reqUrl string
	}{
		{"https://localhost:8444"},
		{"https://127.0.0.1:8444"},
	}

	for _, tt := range tests {
		resp, err := viper.RequestViaTransport(t, client.ln.Addr().String(), tt.reqUrl)
		if err != nil {
			t.Fatal("TestSuccessAuth", err)
		}
		t.Log(resp.StatusCode, err)
		t.Log(resp.Proto)
		resp.Body.Close()
		time.Sleep(2 * time.Second)
	}
}

func TestInvalidAddrType(t *testing.T) {
	server := startServer(newServerDefaultConf(), t)
	defer server.ln.Close()
	config := newClientDefaultConf()

	strategys := []int{0, 2}
	for _, s := range strategys {
		config.ServerAddr = server.ln.Addr().String()
		config.RouteStrategy = s
		client := startClient(config, t)

		target, err := net.Dial("tcp", client.ln.Addr().String())
		if err != nil {
			t.Fatal(err)
		}

		target.Write([]byte{5, 2, 0, 2})

		recv := make([]byte, 2)
		io.ReadAtLeast(target, recv, 2)
		t.Log("auth method", recv)

		target.Write([]byte{5, 1, 0, 5, 14, 215, 177, 39, 80, 0})

		b := make([]byte, 100)
		n, err := io.ReadFull(target, b)
		t.Log(n, err, b[:n])
		if err == nil {
			t.Fatal("TestInvalidAddrType fail")
		}
		client.ln.Close()

		socks5.BlockIps.Delete("127.0.0.1")
		socks5.BlockIps.Delete("::1")
	}
}

func makeBothStart(t *testing.T, sc *config.KcpServerConfig, cc *config.KcpClientConfig) (*kcpServer, *kcpClient) {
	done := make(chan struct{})
	ch := make(chan struct{})
	server := NewKcpServer(sc)
	var client *kcpClient

	go func() {
		server.Serve()
	}()
	go func() {
		<-server.done
		t.Log("server start", server.ln.Addr().String())
		cc.ServerAddr = server.ln.Addr().String()
		client = NewKcpClient(cc)
		ch <- struct{}{}
		client.Serve()
	}()
	go func() {
		<-ch
		<-client.done
		done <- struct{}{}
	}()
	<-done
	t.Log("client start")
	return server, client
}

func startServer(serverConfig *config.KcpServerConfig, t *testing.T) *kcpServer {
	serverConfig.DnsCleanInterval = 60
	server := NewKcpServer(serverConfig)
	go func() {
		server.Serve()
	}()
	<-server.done
	t.Log("server started")
	return server
}

func startClient(clientConfig *config.KcpClientConfig, t *testing.T) *kcpClient {
	client := NewKcpClient(clientConfig)
	go func() {
		client.Serve()
	}()
	<-client.done
	t.Log("client started")
	return client
}
