package kcp

import (
	"crypto/sha1"
	"net"
	"strconv"

	"github.com/xtaci/kcp-go/v5"
	"github.com/xtaci/smux"
	"gitlab.com/RandomCivil/common/log"
	sn "gitlab.com/RandomCivil/common/signal"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/network"
	"gitlab.com/RandomCivil/viper/socks5"
	"golang.org/x/crypto/pbkdf2"
)

type kcpServer struct {
	signal *sn.Signal
	config *config.KcpServerConfig
	done   chan struct{}
	ln     net.Listener
	dc     *network.DnsCache
}

func NewKcpServer(config *config.KcpServerConfig) *kcpServer {
	dc := network.NewDnsCache(config.DnsCleanInterval, config.DnsCleanInterval)
	dc.SetDnsPriority(config.PreferIpv6 == 1)

	s := &kcpServer{
		signal: sn.NewSignal(),
		config: config,
		done:   make(chan struct{}, 1),
		dc:     dc,
	}
	return s
}

func (k *kcpServer) handleConn(conn net.Conn) {
	session, _ := smux.Server(conn, nil)
	defer session.Close()

	wrapSess := &socks5.Session{
		Se:       &network.SmuxSession{Se: session},
		DnsCache: k.dc,
	}
	socks5.HandleServerConn(wrapSess, &socks5.PlainStream{})
}

func (k *kcpServer) Serve() {
	key := pbkdf2.Key([]byte(k.config.Pwd), []byte("demo salt"), 4096, 32, sha1.New)
	block, _ := kcp.NewAESBlockCrypt(key)
	if listener, err := kcp.ListenWithOptions(":"+strconv.Itoa(k.config.Listen), block, 10, 3); err == nil {
		k.ln = listener

		k.done <- struct{}{}

		go k.signal.Handle(listener)
		for {
			conn, err := listener.AcceptKCP()
			if err != nil {
				log.DefaultLogger.Errorf("accept err:%v", err)
				return
			}
			conn.SetStreamMode(true)
			conn.SetWriteDelay(false)
			conn.SetNoDelay(k.config.NoDelay, k.config.Interval, k.config.Resend, k.config.NoCongestion)
			conn.SetWindowSize(k.config.SndWnd, k.config.RcvWnd)
			conn.SetMtu(1350)
			conn.SetACKNoDelay(false)
			go k.handleConn(conn)
		}
	} else {
		log.DefaultLogger.Errorf("listen kcp err:%v", err)
	}
}
