package kcp

import (
	"context"
	"crypto/sha1"
	"io"
	"net"
	"runtime"
	"strconv"
	"time"

	"github.com/xtaci/kcp-go/v5"
	"gitlab.com/RandomCivil/common/log"
	sn "gitlab.com/RandomCivil/common/signal"
	"gitlab.com/RandomCivil/viper"
	"gitlab.com/RandomCivil/viper/config"
	"gitlab.com/RandomCivil/viper/network"
	"gitlab.com/RandomCivil/viper/socks5"
	"golang.org/x/crypto/pbkdf2"
)

type kcpClient struct {
	dialFunc func(ctx context.Context, i int32) *network.DialTarget
	signal   *sn.Signal
	sess     *network.Session
	config   *config.KcpClientConfig
	done     chan struct{}
	ln       net.Listener
	dc       *network.DnsCache
	router   *socks5.Router
}

func NewKcpClient(conf *config.KcpClientConfig) *kcpClient {
	r := socks5.NewRouter(conf)

	dc := network.NewDnsCache(conf.DnsCleanInterval, conf.DnsCleanInterval)
	dc.SetDnsPriority(false)

	c := &kcpClient{
		signal: sn.NewSignal(),
		config: conf,
		done:   make(chan struct{}, 1),
		dc:     dc,
		router: r,
	}

	sessConfig := &network.SessionConfig{
		Strategy: conf.ConnStrategy,
		Num:      conf.ConnNum,
	}
	dialFunc := func(ctx context.Context, i int32) *network.DialTarget {
		r := new(network.DialTarget)
		key := pbkdf2.Key([]byte(conf.Pwd), []byte("demo salt"), 4096, 32, sha1.New)
		block, _ := kcp.NewAESBlockCrypt(key)
		if kcpSess, err := kcp.DialWithOptions(conf.ServerAddr, block, 10, 3); err == nil {

			kcpSess.SetStreamMode(true)
			kcpSess.SetWriteDelay(false)
			kcpSess.SetNoDelay(conf.NoDelay, conf.Interval, conf.Resend, conf.NoCongestion)
			kcpSess.SetWindowSize(conf.SndWnd, conf.RcvWnd)
			kcpSess.SetMtu(1350)
			kcpSess.SetACKNoDelay(false)

			r.Target = kcpSess
			r.Done = true
			r.Conf = &config.SmuxConfig{
				SmuxKeepAliveInterval: conf.SmuxKeepAliveInterval,
				SmuxKeepAliveTimeout:  conf.SmuxKeepAliveTimeout,
			}
		}
		return r
	}
	c.dialFunc = dialFunc

	session, _ := network.NewSession(sessConfig, dialFunc)
	c.sess = session
	return c
}

func (k *kcpClient) clientConn(netConn net.Conn) {
	netConn.SetDeadline(time.Now().Add(60 * time.Second))
	defer netConn.Close()

	rc := &socks5.RouteConn{
		NetConn:   netConn,
		Strategy:  k.config.RouteStrategy,
		Router:    k.router,
		DnsCache:  k.dc,
		ReturnCh:  make(chan struct{}, 1),
		PipeCh:    make(chan *socks5.PipeConn, 1),
		BufConnCh: make(chan io.Reader, 1),
	}
	var concreteConn io.Reader
	socks5.HandleClientConn(rc)
	select {
	case <-rc.ReturnCh:
		return
	case pipeConn := <-rc.PipeCh:
		concreteConn = pipeConn.Pr
		defer pipeConn.Pw.Close()
		defer pipeConn.Pr.Close()
	case bufConn := <-rc.BufConnCh:
		concreteConn = bufConn
	}

	session := k.sess.Fetch()
	if session == nil {
		return
	}
	stream, err := session.OpenStream()
	//session closed
	if err != nil {
		log.DefaultLogger.Errorf("stream open err:%v", err)
		session, stream = k.makeStream(session, k.config.ConnStrategy)
	}

	k.sess.Ch <- session
	defer session.CloseStream(stream)

	errCh := make(chan error, 2)
	go network.Pipe(stream, concreteConn, errCh)
	go network.Pipe(netConn, stream, errCh)
	err = <-errCh
	log.DefaultLogger.Infof("stream in session %d pipe err %v", session.GetIndex(), err)
}

func (k *kcpClient) makeStream(session viper.SessionInterface, strategy string) (viper.SessionInterface, io.ReadWriteCloser) {
	for {
		newSess := network.MakeSession(context.Background(), session.GetIndex(), k.dialFunc)
		if newSess == nil {
			log.DefaultLogger.Infof("create %s session %d fail", strategy, session.GetIndex())
			continue
		}
		session.SetConcreteSession(newSess)
		stream, err := session.OpenStream()
		log.DefaultLogger.Errorf("make stream err:%v", err)
		if err == nil {
			return session, stream
		}
	}
}

func (k *kcpClient) Serve() {
	runtime.GC()
	l, err := net.Listen("tcp", ":"+strconv.Itoa(k.config.Listen))
	if err != nil {
		log.DefaultLogger.Errorf("listen err:%v", err)
		return
	}
	k.ln = l
	log.DefaultLogger.Infof("client start at %s", l.Addr().String())

	k.done <- struct{}{}

	go k.signal.Handle(l)
	for {
		conn, err := l.Accept()
		if err != nil {
			log.DefaultLogger.Errorf("accept err:%v", err)
			return
		}
		go k.clientConn(conn)
	}
}
